<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/css/OrderManagement/app.css">
    <script defer src="{{ asset('js/app.js') }}"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body>
<header id="om-header">

    </header>
    <nav id="om-nav" class="nav">
</nav>

<main class="container" id="om-roostercreate">
    </main>
</body>
</html>