import React from 'react';
import moment from 'moment';

export default class ReserverenHeader extends React.Component{
    state = {showCalendar: false}
    onClickAddOneDay = () => {
        this.props.onClickAddOneDay();
    }
    onClickRemoveOneDay = () => {
        this.props.onClickRemoveOneDay();
    }
    showCalendar = () => {
        this.setState({
            showCalendar: true
        })
    }
    hideCalendar = () => {
        this.setState({
            showCalendar: false
        })
    }
    selectDate = (date) => {
        console.log("Ja")
        event.preventDefault();
        this.props.selectDate(date);
        this.hideCalendar();
    }
    createCalendar = () => {
        let calendarItems = []

        for(let i = 1; i < 8; i++){
            calendarItems.push(<button onClick={() => {this.selectDate(moment().add(i,'day').format('YYYYMMDD'))}} className={moment().add(i,'day').format('YYYYMMDD') == this.props.selectedDate ? "reserveren__header__calendar__date reserveren__header__calendar__date--active" : "reserveren__header__calendar__date"}>
                <section className="reserveren__header__calendar__date__day" >
                    {moment().add(i,'day').format('DD')}
                </section>
                <section className="reserveren__header__calendar__date__month">
                    {moment().add(i,'day').format('MMM')}
                </section>
                <section className="reserveren__header__calendar__date__year">
                    {moment().add(i,'day').format('YYYY')}
                </section>
            </button>);
        }
        calendarItems.push(<button onClick={() => {this.selectDate(moment().format('YYYYMMDD'))}} className={moment().format('YYYYMMDD') == this.props.selectedDate ? "reserveren__header__calendar__today reserveren__header__calendar__today--active" : "reserveren__header__calendar__today"}>Vandaag</button>)
        return calendarItems;
    }
    render(){
        let calendar = null
        if(this.state.showCalendar){
            calendar = <article className="reserveren__header__calendar" display="true">{this.createCalendar()}</article>
        }
    return (
        <header className="reserveren__header">
            <button onClick={this.onClickRemoveOneDay} className={this.props.selectedDate > moment().format('YYYYMMDD') ? "reserveren__header__previousDay material-icons" : "reserveren__header__previousDay reserveren__header__previousDay--hide material-icons"}>
            navigate_before            
            </button>
            <section className="reserveren__header__title">
                <button onClick={this.showCalendar} className=" reserveren__header__title__icon material-icons">
                calendar_today
                </button>
                {this.props.dateTitle}
            </section>
            <button onClick={this.onClickAddOneDay} className={this.props.selectedDate < moment().add(1, 'week').format('YYYYMMDD') ? "reserveren__header__nextDay material-icons" : "reserveren__header__previousDay reserveren__header__nextDay--hide material-icons"}>
            navigate_next                
            </button>
            {calendar}
        </header>
    );
    }
}