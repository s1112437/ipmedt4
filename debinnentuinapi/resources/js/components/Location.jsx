import React from 'react';


export default class Location extends React.Component {
    selectLocation = () => {
        this.props.selectLocation(this.props.locationName);
    }
    render(){
    return (
        <button onClick={this.selectLocation} className={this.props.styling}>
            {this.props.locationName}
        </button>
    )
    }
}