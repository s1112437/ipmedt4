import React from 'react';
import ReactDOM from 'react-dom';
import ReserverenHeader from './ReserverenHeader.jsx';
import Location from './Location.jsx';
import AantalPersonen from './AantalPersonen.jsx';
import Tijdslot from './Tijdslot.jsx';
import ReservateButton from './ReservateButton.jsx';
import moment from 'moment'

import axios from "axios";


export default class Reserveren extends React.Component{
    
    state = { locations: [], aantalPersonen: 1, selectedLocation: 1, selectedTimeslot: null, selectedDate: moment().format("YYYYMMDD"), tijdSloten: [], available: []};
    componentDidMount() {
    axios.get("/locations").then(res => {
        res.data.forEach((locatie) => {
            this.setState({
                locations: [ ...this.state.locations, {Name: locatie.Locatie}]
            })
        })

    });
    this.getTimeSlots();
    
};
    onAddPerson = () => {
        this.setState({
           aantalPersonen: this.state.aantalPersonen + 1
        }, () => this.checkAvailabilty())
    }
    onRemovePerson = () => {
        if(this.state.aantalPersonen > 1){
            this.setState({
                aantalPersonen: this.state.aantalPersonen - 1
            }, () => this.checkAvailabilty())
        }
    }
    selectLocation = (locationName) => {
        this.state.locations.forEach((location, index) => {
            if(location.Name == locationName){
                this.setState({
                    selectedLocation: index + 1 
                }, function(){
                    this.getTimeSlots();
                })
            }
        })
        
    }
    getTimeSlots = () => {
        this.setState({
            tijdSloten: []
        });
        axios.get("/timeslots/" + this.state.selectedLocation + "/" + this.state.selectedDate).then(res => {
            res.data.forEach(tijdslot => {
                this.setState({
                    tijdSloten: [ ...this.state.tijdSloten, tijdslot]
                }, () => this.checkAvailabilty());
            });
        });
        
    };
    addOneDay = () => {
        if(moment(this.state.selectedDate).add(1, 'day') < moment().add(1, 'week')){
            this.setState({
                selectedDate:  moment(this.state.selectedDate).add(1, 'day').format("YYYYMMDD")
            }, () => {
                this.getTimeSlots()
            });
        }
    }
    removeOneDay = () => {
            this.setState({
                selectedDate:  moment(this.state.selectedDate).add(-1, 'day').format("YYYYMMDD")
            }, () => {
                this.getTimeSlots()
            });

    }
    selectDate = (date) => {
        this.setState({
            selectedDate: date
        }, () => {
            this.getTimeSlots()
        })
    }
    selectTijdslot = (tijdslotId) => {
        this.setState({
            selectedTimeslot: tijdslotId
        }, () => { this.getAvailability()})
    }
    checkAvailabilty = () => {
        let newTijdSloten = this.state.tijdSloten;
        this.state.tijdSloten.map((tijdslot, key) => {
            newTijdSloten[key] = { ...newTijdSloten[key], isAvailable: true}
            if(tijdslot.aantalPersonen < this.state.aantalPersonen){
                newTijdSloten[key] = { ...newTijdSloten[key], isAvailable: false}
            }
            else if(tijdslot.beschikbaar == 0){
                newTijdSloten[key] = { ...newTijdSloten[key], isAvailable: false}
            }
            else if(tijdslot.startTijd < moment().format("HH:MM:SS") && this.state.selectedDate == moment().format("YYYYMMDD")){
                newTijdSloten[key] = { ...newTijdSloten[key], isAvailable: false}
            }
            
        })
        this.setState({
            tijdSloten: newTijdSloten,
        });
        this.setState({
            selectedTimeslot: null,
        });

    }
    getAvailability(){
        axios.get('/checkAvailability/' + this.state.selectedTimeslot  +'/' + this.state.aantalPersonen).then((res) => {
            this.setState({
                available: res.data
            });
        });
    }
    makeReservation = (selectedTimeslot, aantalPersonen) => {
        axios.post('/reserveringen', {selectedTimeslot: selectedTimeslot, aantalPersonen: aantalPersonen })
        .then((res) => {
            if(res.data.hasOwnProperty('Available')){
                this.setState({
                    available: res.data
                })
            }
        })
        this.getTimeSlots();
        
    }
    render(){
        let reserverenHeader;
        let tijdsloten;
        switch(this.state.selectedDate){
            case moment().format("YYYYMMDD"):
                reserverenHeader = <ReserverenHeader dateTitle="Vandaag" selectedDate={this.state.selectedDate} selectDate={this.selectDate} onClickAddOneDay={this.addOneDay} onClickRemoveOneDay={this.removeOneDay} />;
                break;
            case moment().add(1, 'day').format("YYYYMMDD"):
                reserverenHeader = <ReserverenHeader dateTitle="Morgen" selectedDate={this.state.selectedDate} selectDate={this.selectDate} onClickAddOneDay={this.addOneDay} onClickRemoveOneDay={this.removeOneDay}/>;
                break;
            default:
                reserverenHeader = <ReserverenHeader dateTitle={moment(this.state.selectedDate).format("DD-MM-YYYY")} selectDate={this.selectDate} selectedDate={this.state.selectedDate} onClickAddOneDay={this.addOneDay} onClickRemoveOneDay={this.removeOneDay}/>;
                break;
        }
        switch(this.state.tijdSloten.length){
            case 0:
                tijdsloten = <i className="reserveren__timeslots__message">Geen tijden op deze datum en locatie beschikbaar</i>
                break;
            default:
                tijdsloten = this.state.tijdSloten.map((tijdslot, index) => {
                    if(tijdslot.id == this.state.selectedTimeslot){
                        return <Tijdslot key={index} selectTijdslot={this.selectTijdslot} tijdslotId={tijdslot.id} startTijd={moment(tijdslot.startTijd, [moment.ISO_8601, 'HH:mm:ss']).format("HH:mm")} eindTijd={moment(tijdslot.eindTijd, [moment.ISO_8601, 'HH:mm:ss']).format("HH:mm")} styling={tijdslot.isAvailable ? "reserveren__timeslots__timeslot reserveren__timeslots__timeslot--active" : "reserveren__timeslots__timeslot reserveren__timeslots__timeslot--notAvailable"}/>;
                    }
                    else{
                        return <Tijdslot  key={index} selectTijdslot={this.selectTijdslot} tijdslotId={tijdslot.id} startTijd={moment(tijdslot.startTijd, [moment.ISO_8601, 'HH:mm:ss']).format("HH:mm")} eindTijd={moment(tijdslot.eindTijd, [moment.ISO_8601, 'HH:mm:ss']).format("HH:mm")} styling={tijdslot.isAvailable ? "reserveren__timeslots__timeslot" : "reserveren__timeslots__timeslot reserveren__timeslots__timeslot--notAvailable"}/>;
                    }
                })
                break;
        }
        


    return (
        <article className="container reserveren">
            
            {reserverenHeader}
            <section className="reserveren__subTitle">
                    Kies uw locatie:
            </section>
            <article className="reserveren__locations">               
                {this.state.locations.map((location, index) => {
                    if(index + 1 == this.state.selectedLocation){
                        return <Location styling='reserveren__locations__location reserveren__locations__location--active' selectLocation={this.selectLocation} key={index} locationName={location.Name}/>;
                    }
                    else {
                        return <Location styling='reserveren__locations__location' selectLocation={this.selectLocation} key={index} locationName={location.Name}/>;
                    }
                })}
            </article>
            <section  className="reserveren__subTitle">
                    Aantal personen:
            </section>
            <AantalPersonen onAddPerson={this.onAddPerson}  onRemovePerson={this.onRemovePerson} aantalPersonen={this.state.aantalPersonen}/>
            <section className="reserveren__subTitle">
                   Kies een tijd:
            </section>
            <article className="reserveren__timeslots"> 
                {tijdsloten}
            </article>
            <ReservateButton makeReservation={this.makeReservation} selectedTimeslot={this.state.selectedTimeslot} aantalPersonen={this.state.aantalPersonen} msg={this.state.available.msg} stylingButton={this.state.available.Available ? "reserveren__reservateButtonContainer__reservateButton" : "reserveren__reservateButtonContainer__reservateButton reserveren__reservateButtonContainer__reservateButton--disable"} />
        </article>
    )
    }
}



if (document.getElementById('reserveren')) {
    ReactDOM.render(<Reserveren />, document.getElementById('reserveren'));
}