import React from 'react';
import moment from 'moment';

const OMRooster = (props) => {
    const roosterList = props.roosterlist.map(rooster => <tr key={rooster}>
        <td>{rooster.name}</td>
        <td>{rooster.Locatie}</td>
        <td>{moment(rooster.startDatum).format("DD-MM-YYYY")}</td>
        <td>{moment(rooster.eindDatum).format("DD-MM-YYYY")}</td>
        <td className="roosters__table__icon">
        <a href={"/admin/rooster/" + rooster.id + "/edit"}  className="material-icons">
create
</a>
<a class="material-icons">
delete
</a>

        </td>
    </tr>)
    return (
        <tbody>
            {roosterList}
        </tbody>
    )
}

export default OMRooster;