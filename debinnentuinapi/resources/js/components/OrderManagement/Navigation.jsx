import React from 'react';
import ReactDOM from 'react-dom';

export default class OMNavigation extends React.Component {
    state = { activeDropdownElmId: null }
    openDropdownMenu = (e) => {
        e.target.childNodes[1].style.display ="flex"
        this.setState({
            activeDropdownElmId: e.target.childNodes[1].id
        })
    }
    closeDropdownMenu = () => {
        document.getElementById(this.state.activeDropdownElmId).style.display = "none";
    }

    render(){
        return(
            <ul className="nav__list" >
                <li className="nav__list__item">
                    Home
                </li>
                <li className="nav__list__item">
                    Klanten
                </li>
                <li className="nav__list__item">
                    Bestellingen
                </li>
                <li onMouseOver={this.openDropdownMenu} onMouseLeave={this.closeDropdownMenu}  className="nav__list__item">
                    Tijden
                    <section id="js--tijdenDropdown" onMouseLeave={this.closeDropdownMenu}  className="nav__list__item__dropdown">
                        <ul  className="nav__list__item__dropdown__list">
                            <a href="/admin/roosters" className="nav__list__item__dropdown__list__item nav__list__item__dropdown__list__item--active">
                                Roosters
                            </a>
                            <li className="nav__list__item__dropdown__list__item">
                                Tijdsloten
                            </li>
                        </ul>
                    </section>

                </li>
                <li className="nav__list__item">
                    Voorkeuren
                </li>
            </ul>
        );
    };
};

if (document.getElementById('om-nav')) {
    ReactDOM.render(<OMNavigation />, document.getElementById('om-nav'));
}