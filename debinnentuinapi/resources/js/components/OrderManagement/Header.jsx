import React from 'react';
import ReactDOM from 'react-dom';

export default function OMHeader(){
    return(
        <section className="header">
            <img src="https://rubenjerry.nl/wp-content/uploads/2019/05/logo_green-300x67.png"></img>
        </section>
    )
};

if (document.getElementById('om-header')) {
    ReactDOM.render(<OMHeader />, document.getElementById('om-header'));
}