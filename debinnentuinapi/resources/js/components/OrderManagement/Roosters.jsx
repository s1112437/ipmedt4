import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import OMRooster from './Rooster.jsx';

export default class OMRoosters extends React.Component {
    state = {roosters: []};
    componentDidMount() {
        axios.get("/roosters").then((res) => {
            
            res.data.forEach((rooster) => {
                
                axios.get("/location/" + rooster.location_id).then((res) => {
                    rooster["Locatie"] = res.data.Locatie;
                    console.log(rooster)
                    this.setState({
                        roosters: [...this.state.roosters, rooster]
                    })
                })

            });
        })
    };
    render(){
        return(
            <article className="container roosters">
                <header className="roosters__header">
                    <h2>Roosters</h2>
                    <a href="/admin/rooster/create"  className="roosters__header__button">
                    <span className="roosters__header__button__icon material-icons">
                        add
                    </span>
                        Rooster toevoegen
                    </a>

                </header>
                <table  className="roosters__table">
                        <thead>
                        <tr>
                            <th scope="col">Naam</th>
                            <th scope="col">Locatie</th>
                            <th scope="col">Rooster startdatum</th>
                            <th scope="col">Rooster einddatum</th>
                            <th scope="col"></th>

                        </tr>

                        </thead>
      
                        <OMRooster roosterlist={this.state.roosters} />


   
   

                        
            
                </table>
            </article>
        )
    }
}

if (document.getElementById('om-roosters')) {
    ReactDOM.render(<OMRoosters />, document.getElementById('om-roosters'));
}