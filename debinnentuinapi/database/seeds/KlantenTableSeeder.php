<?php

use Illuminate\Database\Seeder;

class KlantenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('klanten')->insert([
            'fname'=> 'Tjalle',
            'lname' => 'Spaan',
            'email'=> 'tjallespaan@gmail.com',
            'phonenumber'=>'0610383526',
            'userid'=> 1
        ]);

    }
}
