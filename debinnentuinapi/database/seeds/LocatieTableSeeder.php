<?php

use Illuminate\Database\Seeder;

class LocatieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location')->insert([
            'Locatie' => 'Café'
        ]);
        DB::table('location')->insert([
            'Locatie' => 'The Roof'
        ]);
    }
}
