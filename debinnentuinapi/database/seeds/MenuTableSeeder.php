<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            'menu_item' => 'Organic Yoghurt',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 4.00,
            'description' => 'with granola & jam',
            'menu_item_id' => 10000
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Daily Two',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 1.50,
            'description' => 'pieces of fruit',
            'menu_item_id' => 10001
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Smoothie',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 3.50,
            'description' => 'with forests fruits & yoghurt',
            'menu_item_id' => 10002
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Breakfast Cocktail',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 3.50,
            'description' => 'with oats mango & banana',
            'menu_item_id' => 10003
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Oat Cookie',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 1.50,
            'description' => 'homemade',
            'menu_item_id' => 10004
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Croissant',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 1.00,
            'description' => 'freshly baked ',
            'menu_item_id' => 10005
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Croissant +',
            'categorie' => 'snacks',
            'beschikbaarheid' => True,
            'prijs' => 1.50,
            'description' => 'freshly baked + butter & jam ',
            'menu_item_id' => 10006
        ]);

        DB::table('menu')->insert([
            'menu_item' => 'Mack & Cheese',
            'categorie' => 'toasts',
            'beschikbaarheid' => True,
            'prijs' => 4.25,
            'description' => 'mackerel melt toastie',
            'menu_item_id' => 10007
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Italian',
            'categorie' => 'toasts',
            'beschikbaarheid' => True,
            'prijs' => 4.25,
            'description' => 'with mozzarella, perso & tomato ',
            'menu_item_id' => 10008
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Classic Cheese',
            'categorie' => 'toasts',
            'beschikbaarheid' => True,
            'prijs' => 3.00,
            'description' => 'toasted cheese sandwich',
            'menu_item_id' => 10009
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Avo on Toast',
            'categorie' => 'toasts',
            'beschikbaarheid' => True,
            'prijs' => 4.75,
            'description' => 'with tomato, pumpkin seeds & parsley',
            'menu_item_id' => 10010
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Toasted Banana Bread',
            'categorie' => 'toasts',
            'beschikbaarheid' => True,
            'prijs' => 4.75,
            'description' => 'with yogurt, walnuts & jam',
            'menu_item_id' => 10011
        ]);

        DB::table('menu')->insert([
            'menu_item' => 'The Goat',
            'categorie' => 'salads',
            'beschikbaarheid' => True,
            'prijs' => 6.00,
            'description' => 'grilled goat cheese, walnuts & honey',
            'menu_item_id' => 10012
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'California',
            'categorie' => 'salads',
            'beschikbaarheid' => True,
            'prijs' => 6.00,
            'description' => 'salmon & avocado',
            'menu_item_id' => 10013
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Mista',
            'categorie' => 'salads',
            'beschikbaarheid' => True,
            'prijs' => 5.50,
            'description' => 'mozarella & balsamic syrup',
            'menu_item_id' => 10014
        ]);
        
        DB::table('menu')->insert([
            'menu_item' => 'Caprese',
            'categorie' => 'sandwiches',
            'beschikbaarheid' => True,
            'prijs' => 3.75,
            'description' => 'pesto, mozzarella, tomato & rocket',
            'menu_item_id' => 10015
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Salmon',
            'categorie' => 'sandwiches',
            'beschikbaarheid' => True,
            'prijs' => 4.00,
            'description' => 'smoked ASC salmon with cream cheese',
            'menu_item_id' => 10016
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Mackerel',
            'categorie' => 'sandwiches',
            'beschikbaarheid' => True,
            'prijs' => 3.75,
            'description' => 'homemade mackerel salad',
            'menu_item_id' => 10017
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Avo',
            'categorie' => 'sandwiches',
            'beschikbaarheid' => True,
            'prijs' => 4.00,
            'description' => 'avocado, hummus, tomato & walnuts',
            'menu_item_id' => 10018
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Hummus',
            'categorie' => 'sandwiches',
            'beschikbaarheid' => True,
            'prijs' => 3.50,
            'description' => 'with tomato & lettuce',
            'menu_item_id' => 10019
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Gezond',
            'categorie' => 'sandwiches',
            'beschikbaarheid' => True,
            'prijs' => 3.50,
            'description' => 'cheese & greens',
            'menu_item_id' => 10020
        ]);

        DB::table('menu')->insert([
            'menu_item' => 'Various Teas',
            'categorie' => 'hot_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.20,
            'description' => '',
            'menu_item_id' => 10021
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Fresh Mint Tea',
            'categorie' => 'hot_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.65,
            'description' => '',
            'menu_item_id' => 10022
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Ginger Tea',
            'categorie' => 'hot_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.65,
            'description' => '',
            'menu_item_id' => 10023
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Hot Chocolate',
            'categorie' => 'hot_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.75,
            'description' => '',
            'menu_item_id' => 10024
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Chai latte',
            'categorie' => 'hot_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.95,
            'description' => '',
            'menu_item_id' => 10025
        ]);

        DB::table('menu')->insert([
            'menu_item' => 'Espresso',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.20,
            'description' => '',
            'menu_item_id' => 10026
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Doppio',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.80,
            'description' => '',
            'menu_item_id' => 10027
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Americano',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.20,
            'description' => '',
            'menu_item_id' => 10028
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Capuccino',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.70,
            'description' => '',
            'menu_item_id' => 10029
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Caffe latte',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.95,
            'description' => '',
            'menu_item_id' => 10030
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Latte macchiato',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.95,
            'description' => '',
            'menu_item_id' => 10031
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Cortado',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.60,
            'description' => '',
            'menu_item_id' => 10032
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Flat White',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 2.95,
            'description' => '',
            'menu_item_id' => 10033
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Filter brew',
            'categorie' => 'coffee',
            'beschikbaarheid' => True,
            'prijs' => 1.75,
            'description' => '',
            'menu_item_id' => 10034
        ]);

        DB::table('menu')->insert([
            'menu_item' => 'Lemonaid',
            'categorie' => 'cold_drinks',
            'beschikbaarheid' => True,
            'prijs' => 3.00,
            'description' => '',
            'menu_item_id' => 10035
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Charitea',
            'categorie' => 'cold_drinks',
            'beschikbaarheid' => True,
            'prijs' => 3.00,
            'description' => '',
            'menu_item_id' => 10036
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Schulp Juice',
            'categorie' => 'cold_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.75,
            'description' => '',
            'menu_item_id' => 10037
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Milk',
            'categorie' => 'cold_drinks',
            'beschikbaarheid' => True,
            'prijs' => 1.95,
            'description' => 'by Arla Organic',
            'menu_item_id' => 10038
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Made Blue Water',
            'categorie' => 'cold_drinks',
            'beschikbaarheid' => True,
            'prijs' => 1.40,
            'description' => '',
            'menu_item_id' => 10039
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Soda Specials',
            'categorie' => 'cold_drinks',
            'beschikbaarheid' => True,
            'prijs' => 2.50,
            'description' => '',
            'menu_item_id' => 10040
        ]);

        DB::table('menu')->insert([
            'menu_item' => 'Kaartje',
            'categorie' => 'theroof',
            'beschikbaarheid' => True,
            'prijs' => 10.00,
            'description' => 'Entree Doordeweeks, 6 strippen',
            'menu_item_id' => 11013
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Kaartje Regular',
            'categorie' => 'theroof',
            'beschikbaarheid' => True,
            'prijs' => 17.00,
            'description' => 'Entree Weekend, 6 strippen + snacks',
            'menu_item_id' => 11014
        ]);
        DB::table('menu')->insert([
            'menu_item' => 'Kaartje Large',
            'categorie' => 'theroof',
            'beschikbaarheid' => True,
            'prijs' => 27.00,
            'description' => 'Entree Weekend, 9 strippen + snacks',
            'menu_item_id' => 11015
        ]);
    }
}
