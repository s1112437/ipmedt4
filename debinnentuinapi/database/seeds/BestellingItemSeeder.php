<?php

use Illuminate\Database\Seeder;

class BestellingItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bestelling_item')->insert([
            'bestelling_item_id' => 10006,
            'bestelling_item_naam' => 'Croissant +',
            'aantal' => 3,
            'prijs_per_stuk' => 1.50,
            'gebruikersnaam' => 'mauriccio',
            'bestelling_id' => 10006
        ]);
    }
}
