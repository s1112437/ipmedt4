<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTijdslotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tijdslot', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rooster_id');
            $table->unsignedBigInteger('dagen_id');
            $table->time('startTijd');
            $table->time('eindTijd');
            $table->integer('aantalPersonen');
            $table->boolean('beschikbaar');

            $table->foreign('rooster_id')->references('id')->on('rooster');
            $table->foreign('dagen_id')->references('id')->on('dagen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tijdslot', function (Blueprint $table) {
            $table->dropForeign('tijdslot_dagen_id_foreign');
            $table->dropForeign('tijdslot_rooster_id_foreign');
        });
        Schema::dropIfExists('tijdslot');
    }
}
