<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBestellingOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bestellingen_orders', function (Blueprint $table) {
            $table->string('bestelling_id_order')->unique();
            $table->unsignedBigInteger('reservering_id');
            $table->string('klant_naam');
            $table->string('status')->default("pending");
            $table->string('extra_info')->nullable();
            $table->double('totaal_prijs');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bestelling_order');
    }
}
