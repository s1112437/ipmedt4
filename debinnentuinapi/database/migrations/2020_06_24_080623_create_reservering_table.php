<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReserveringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservering', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('tijdslot_id');
            $table->integer('aantalPersonen')->default(1);
            $table->unsignedBigInteger('klanten_id')->nullable();
            $table->string('bestelling_id')->nullable();
            $table->unsignedBigInteger('tables_id')->nullable();
            $table->string('Status');

            $table->foreign('tables_id')->references('id')->on('table');
            $table->foreign('tijdslot_id')->references('id')->on('tijdslot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservering', function (Blueprint $table) {
            $table->dropForeign('reservering_tables_id_foreign');
        });
        Schema::table('reservering', function (Blueprint $table) {
            $table->dropForeign('reservering_tijdslot_id_foreign');
        });
        Schema::dropIfExists('reservering');
    }
}
