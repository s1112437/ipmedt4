<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpeningstijdenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openingstijden', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rooster_id');
            $table->string('Dag');
            $table->time('startTijd');
            $table->time('eindTijd');

            $table->foreign('rooster_id')->references('id')->on('rooster');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('openingstijden', function (Blueprint $table) {
            $table->dropForeign('openingstijden_rooster_id_foreign');
        });
        Schema::dropIfExists('openingstijden');
    }
}
