<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/email', function (){
//     Mail::to('email@email.com')->send(new PasswordReset());


//     return new PasswordReset();
// });


Route::get('/reserveren', function(){
    return view('reserveren');
});

Auth::routes(['verify' => true]);


Route::get('/admin/rooster/create', function(){
    return view('OrderManagement/roostercreate');
});

Route::get('/admin/roosters', function(){
    return view('OrderManagement/roosters');
});

Route::get('/home', function () {
    return view('home');
});
Route::get('/register', function () {
    return view('auth/register');
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home') ->middleware('verified');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
