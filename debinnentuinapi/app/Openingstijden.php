<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Openingstijden extends Model
{
    protected $table = 'openingstijden';
    public $timestamps = false;

    public function rooster(){
        return $this->belongsTo('App\Rooster');
    }
}
