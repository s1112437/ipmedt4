<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dagen extends Model
{
    protected $table = 'dagen';
    public $timestamps = false;

    public function tijdsloten(){
        return $this->hasMany('App\TijdSloten');
    }
}
