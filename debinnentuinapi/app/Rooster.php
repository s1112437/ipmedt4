<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rooster extends Model
{
    protected $table = 'rooster';
    public $timestamps = false;

    public function location(){
        return $this->belongsTo('App\Location');
    }
    public function Openingstijden(){
        return $this->hasMany('App\Openingstijden');
    }
    public function tijdsloten(){
        return $this->hasMany('App\TijdSloten');
    }
}
