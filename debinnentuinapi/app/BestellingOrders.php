<?php

namespace App;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class BestellingOrders extends Model
{
    protected $table = 'bestellingen_orders';

    protected function serializeDate(DateTimeInterface $date){
        return $date->format('Y-m-d H:i:s');
    }
}
