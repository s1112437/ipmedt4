<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Klanten;
use App\Reserveringen;
use App\TijdSloten;
use App\Dagen;
use DateTime;
class KlantenController extends Controller
{
    public function show($klantId){
        return Klanten::find($klantId);
    }
    
    public function getReserveringen($klantId){
        $reserveringen = Klanten::find($klantId)->Reserveringen();
        $now = new DateTime();
        $nowTime = $now->format('H:i:s');
        $nowDate = $now->format('Y-m-d');
        
        $dag = Dagen::where('Datum','=',$nowDate)->first()->id;
        $reserveringen = $reserveringen->whereHas("tijdslot", function(Builder $query) use ($nowTime, $dag) {
            $query->where("tijdslot.startTijd","<=",$nowTime)->where('tijdslot.dagen_id','=', $dag);
        })->get();
        return  $reserveringen;
    }

    public function store(Request $request){
        $klant = new Klanten();
        $klant->name = $request->geenUsername;
        $klant->email = $request->geenUseremail;
        $klant->phone = $request->geenUserphone;
        $klant->save();
        return response()->json([
            "klantId" => $klant->id
        ]);
    }
    public function getKlant(){
        $klant = Klanten::where('email', '!=', null)->get();
        return $klant;
    }

}
