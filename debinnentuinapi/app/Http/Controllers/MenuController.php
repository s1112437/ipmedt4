<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    public function show($categorie){
        $menu = Menu::where('categorie','=',$categorie)->where('beschikbaarheid','!=', 0)->get();
        return $menu;
    }
}
