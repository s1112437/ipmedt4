<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rooster;
use App\Openingstijden;
use App\Dagen;


class OpeningstijdenController extends Controller
{
    public function getOpeningstijden($roosterId){
        return Rooster::find($roosterId)->Openingstijden()->get();
    }

    public function getOpeningstijdenByLocation($location, $day){
        return Rooster::where("location_id","=",$location)->first()->Openingstijden()->where("Dag","=",$day)->get();
    }
}
