<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use DateTime;
use DateInterval;
use App\Mail\PasswordReset;
use App\User;
use App\PasswordResets;


class PasswordResetController extends Controller
{
    public function sendEmail(Request $request){

        $passwordReset = new PasswordResets();
        $passwordReset->email = $request->email;
        $passwordReset->token = Str::random(60);
        $passwordReset->save();


        if(User::where('email', '=', $request->email)->count() >= 1){
            $details = [
                'title'  => 'Je wachtwoord resetten?',
                'body'   => 'Als je een aanvraag voor een wachtwoord reset hebt gemaakt, klik op de onderstaande knop.
                             Als je deze aanvraag niet hebt gemaakt, negeer deze e-mail.',
                'button' => 'Wachtwoord resetten',
                'token'  => $passwordReset->token
            ];
            Mail::to($request->email)->send(new \App\Mail\PasswordReset($details));
            return response()->json([
                "error" => false, 
                "message" => "Er is een wachtwoord reset mail naar " . $request->email . " gestuurd."
            ]);
        }else{
            return response()->json([
                "error" => true, 
                "message" => "Dit email adres is niet bij ons bekend."
            ]);
        }


    }
    public function veranderWachtwoord(Request $request){
        $user = User::find($request->user_id);
        $newPassword = Hash::make($request->newPassword);
        if (Hash::check($request->confirmNewPassword,  $newPassword)){
            $user->password = $newPassword;
            $user->save();
        }
        else{
            echo "not valid";
        }     
    }

    public function resetWachtwoord(Request $request){
        $resetPassword = PasswordResets::where('token','=',$request->token)->first();
        $created_at = new DateTime($resetPassword->created_at);
        $validuntil = $created_at->add(new DateInterval('PT' . 30 . 'M'));
        if(new DateTime() < $validuntil){
            $user = User::where('email','=',$resetPassword->email)->first();
            $newPassword = Hash::make($request->newPassword);
            if (Hash::check($request->confirmNewPassword,  $newPassword)){
                $user->password = $newPassword;
                $user->save();
                PasswordResets::where('email','=',$resetPassword->email)->delete();
            }
        }
        else{
            echo "not valid";
        }
    }

    public function checkValid($token){
        if(PasswordResets::where('token','=',$token)->count() > 0){
            $resetPassword = PasswordResets::where('token','=',$token)->first();
            $created_at = new DateTime($resetPassword->created_at);
            $validuntil = $created_at->add(new DateInterval('PT' . 30 . 'M'));
            if(new DateTime() > $validuntil){
                return response()->json([
                    "error" => true,
                    "message" => "Token is verlopen"
                ]);
            }
        }
        else{
            return response()->json([
                "error" => true,
                "message" => "Token bestaat niet meer"
            ]);
        }
       
    }




}
