<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;

class MollieHandler extends Controller
{
  public function preparePayment($orderId, $amount){
    $payment = Mollie::api()->payments->create([
        "amount" => [
            "currency" => "EUR",
            "value" => $amount
        ],
        "description" => "Order: #" . $orderId,
        "redirectUrl" => "http://127.0.0.1:3000/succesfullpayment",
    ]);

    $payment = Mollie::api()->payments->get($payment->id);

    return $payment->getCheckoutUrl();
}
}
