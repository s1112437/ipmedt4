<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Tables;
use App\Location;
class TablesController extends Controller
{
    public function index($locationId){
        return Tables::where('location_id','=',$locationId)->get();
    }
    public function store(Request $request){
        $table = new Tables();
        $location = Location::find($request->locationId);
        $table->name = $request->name;
        $table->Location()->associate($location);
        $table->save();

        return $table->id;
    }
    public function show($tableId){
        return Tables::find($tableId);
    }
    public function getAvailableTables($tijdslotId, $locationId){
        return Tables::whereDoesntHave('reservering', function(Builder $query) use ($tijdslotId) {
            $query->where('reservering.tijdslot_id','=', $tijdslotId);
        })->where('location_id','=', $locationId)->get();
    }
}
