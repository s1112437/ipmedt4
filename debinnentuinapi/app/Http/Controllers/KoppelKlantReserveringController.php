<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserveringen;

class KoppelKlantReserveringController extends Controller
{
    public function store(Request $request) {
        $reservering = Reserveringen::find($request->reservering_id);

        // Ophalen existing database
        $reservering->klanten_id = $request->klant_id;
        $reservering->bestelling_id = $request->bestel_id;

        $reservering->save();
        return $reservering;
    }
}
