<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Bestellingen;
use App\BestellingOrders;

class BestellingenController extends Controller
{
    public function store(Request $request){
            $data = $request->all();
            $randomId = Str::random(13);
            // evt validator functie toevoegen die checkt of de random gegenereerde string al bestaat in de bestellingentabel.
            $count = 0;
            $bestellingenData = [];
            foreach($data[0] as $request_item){
                if($count === 0){
                    $bestelling_orders = new BestellingOrders();
                    $bestelling_orders->bestelling_id_order = $randomId;
                    $bestelling_orders->reservering_id = $data[3]; //evt reserveringsid dmv redux state halen
                    $bestelling_orders->klant_naam = "test"; //evt klant name dmv redux state halen
                    $bestelling_orders->extra_info = $data[1]; //evt klant name dmv redux state halen
                    $bestelling_orders->totaal_prijs = $data[2];
                    $bestelling_orders->save();
                }
                $bestelling_item = new Bestellingen();
                $bestelling_item->bestelling_item_id = $request_item['menu_id'];
                $bestelling_item->bestelling_item_naam = $request_item['menu_item'];
                $bestelling_item->aantal = $request_item['quantity'];
                $bestelling_item->prijs_per_stuk = $request_item['menu_item_prijs'];

                $bestelling_item->bestelling_id = $randomId;

                $bestellingenData[] = $bestelling_item;
                $bestelling_item->save();
                $count = $count + 1;
            }
            return $bestelling_orders;
    }

    public function getOrderId(){
      $results = Bestellingen::latest('created_at')->first();
      $orderid = $results->bestelling_id;
      return $orderid;
    }
    public function getPrice(){
      $results = Bestellingen::latest('created_at')->first();
      $orderid = $results->bestelling_id;
      $prices = Bestellingen::all()->where('bestelling_id', $orderid)->pluck('prijs_per_stuk');
      $qty = Bestellingen::all()->where('bestelling_id', $orderid)->pluck('aantal');
      $hits = count($qty);
      $totalArray = array();
      for($i = 0; $i < $hits; $i++){
        $times = $prices[$i] * $qty[$i];
        array_push($totalArray, $times);
      }
      $totalInt = array_sum($totalArray);
      $totalDouble = number_format($totalInt,2);


      return $totalDouble;
    }
}
