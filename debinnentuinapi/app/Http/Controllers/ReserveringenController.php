<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserveringen;
use App\TijdSloten;
use App\Dagen;
use App\Rooster;
use App\Jobs\expireReservation;
use Illuminate\Database\Eloquent\Builder;
use DateTime;

class ReserveringenController extends Controller
{
    public function store(Request $request){
        date_default_timezone_set("Europe/Amsterdam");
        $tijdslot = TijdSloten::find($request->selectedTimeslot);
        // var_dump($tijdslot);
        $dag = $tijdslot->Dagen()->pluck('Datum')->first();
        if(new DateTime() > new DateTime($dag . $tijdslot->startTijd)){
            return response()->json([
                "Available" => false,
                "msg" => "Gekozen tijd is niet meer beschikbaar"
            ]);
        }
        else if($tijdslot->aantalPersonen < $request->aantalPersonen){
            return response()->json([
                "Available" => false,
                "msg" => "Gekozen tijd is niet meer beschikbaar voor het aantal gekozen personen"
            ]);
        }
        else{
            $tijdslot->aantalPersonen -= $request->aantalPersonen;
            // var_dump( $request->aantalPersonen);
            $tijdslot->save();
            // var_dump($tijdslot->wasChanged('aantalPersonen'));

            $reservering = new Reserveringen();
            $reservering->Tijdslot()->associate($tijdslot);
            $reservering->aantalPersonen = $request->aantalPersonen;
            $reservering->klanten_id = null;
            $reservering->bestelling_id = null;
            $reservering->Status = "Concept";
            $reservering->save();
            // echo $reservering;
            $reserveringId = $reservering->id;
            // expireReservation::dispatch($reserveringId)->delay(now()->addseconds(10));
            // var_dump(time() + 10);
            return $reservering->id;
        }
    }
    public function getReserveringenByTime($startTijd, $date, $locationid){
        $dag = Dagen::where("Datum","=",date("Y-m-d" , strtotime($date)))->first();
        $duurTijdslot = $dag->tijdsloten()->first()->Rooster()->pluck("duurTijdSlot");
        // echo(date('H:m', strtotime($startTijd)));
        // echo(date("H:m", strtotime($startTijd)));
        $eindTijd = date("H:i:s", strtotime($startTijd) + strtotime($duurTijdslot[0]));
        // echo(date('H:m', (strtotime($startTijd) . strtotime($tussentijd))));
        $rooster = Rooster::where('location_id','=',$locationid)->pluck("id");
        $tijdsloten = $dag->tijdsloten()->whereIn("rooster_id", $rooster)->pluck("id");
        $reserveringen = Reserveringen::whereHas("tijdslot", function(Builder $query) use ($startTijd, $eindTijd) {
            $query->where("tijdslot.startTijd",">=",$startTijd)->where("tijdslot.startTijd", '<', $eindTijd);
        })->whereIn("tijdslot_id",$tijdsloten)->get();
        echo($reserveringen);
        // $tussentijd = $tijdsloten->Rooster();
        // $tijdsloten->where("startTijd","=",$starttijd)->where("eindTijd")

    }

    public function getReserveringenByTimeslot($datetime){
        $nowTime = Date("H:i:s", strtotime($datetime));
        $dag = Dagen::where("Datum","=",Date("yy-m-d", strtotime($datetime)))->get();
        $reserveringen = Reserveringen::whereHas("tijdslot", function(Builder $query) use ($nowTime) {
            $query->where("tijdslot.startTijd","<=",$nowTime)->where("tijdslot.eindTijd", '>=', $nowTime);
        })->get();
        
        return $reserveringen;
    }
    public function getReserveringenByTable($tableId, $datetime){
        $nowTime = Date("H:i:s", strtotime($datetime));
        $dag = Dagen::where("Datum","=",Date("yy-m-d", strtotime($datetime)))->get();
        $reserveringen = Reserveringen::whereHas("tijdslot", function(Builder $query) use ($nowTime) {
            $query->where("tijdslot.startTijd","<=",$nowTime)->where("tijdslot.eindTijd", '>=', $nowTime);
        })->where("tables_id",'=',$tableId)->get();
        
        return $reserveringen;
    }
    public function show($reserveringId){
        $tijdslot = Reserveringen::find($reserveringId)->Tijdslot()->first()->id;
        return response()->json(['Reservering' => Reserveringen::find($reserveringId),'timeslots' => 'http://localhost:8000/api/timeslot/' . $tijdslot]);
    }

    public function setTable($reserveringId, $tableId){
        $reservering = Reserveringen::find($reserveringId);
        $reservering->tables_id = $tableId;
        $reservering->save();
    }
}
