<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rooster;
use App\Openingstijden;
use App\Location;
class RoostersController extends Controller
{
    public function index(){
        return Rooster::all();
    }

    public function edit($roosterId){
        return View('/OrderManagement/roosteredit')->with('roosterId', $roosterId);
    }
    public function save(Request $request){
        $rooster = Rooster::find($request->id);
        $rooster->name = $request->name;
        $location = Location::find($request->location_id);
        $rooster->location()->associate($location);
        if($request->startDatum && $request->eindDatum){
            $rooster->startDatum = $request->startDatum;
            $rooster->eindDatum = $request->eindDatum;
        }
        else{
            $rooster->oneindig = $request->oneindig;
        }
        $rooster->aantalPersonen = $request->aantalPersonen;
        $rooster->duurTijdSlot = $request->duurTijdSlot;
        $rooster->duurTussenTijdSlot = $request->duurTussenTijdSlot;
        $rooster->save();

        $monday = $rooster->Openingstijden()->where('Dag','=','Monday')->first();
        if($request->monday_openingstijd && $request->monday_sluitingstijd){
            $monday->startTijd = $request->monday_openingstijd;
            $monday->eindTijd = $request->monday_sluitingstijd;
        }
        else{
            $monday->startTijd = "00:00:00";
            $monday->eindTijd = "00:00:00";  
        }
        $monday->save();


        $tuesday = $rooster->Openingstijden()->where('Dag','=','Tuesday')->first();
        if($request->tuesday_openingstijd && $request->tuesday_sluitingstijd){
            $tuesday->startTijd = $request->tuesday_openingstijd;
            $tuesday->eindTijd = $request->tuesday_sluitingstijd;
        }
        else{
            $tuesday->startTijd = "00:00:00";
            $tuesday->eindTijd = "00:00:00";  
        }
        $tuesday->save();


        $wednesday = $rooster->Openingstijden()->where('Dag','=','Wednesday')->first();
        if($request->wednesday_openingstijd && $request->wednesday_sluitingstijd){
            $wednesday->startTijd = $request->wednesday_openingstijd;
            $wednesday->eindTijd = $request->wednesday_sluitingstijd;
        }
        else{
            $wednesday->startTijd = "00:00:00";
            $wednesday->eindTijd = "00:00:00";  
        }
        $wednesday->save();


        $thursday = $rooster->Openingstijden()->where('Dag','=','Thursday')->first();
        if($request->thursday_openingstijd && $request->thursday_sluitingstijd){
            $thursday->startTijd = $request->thursday_openingstijd;
            $thursday->eindTijd = $request->thursday_sluitingstijd;
        }
        else{
            $thursday->startTijd = "00:00:00";
            $thursday->eindTijd = "00:00:00";     
        }
        $thursday->save();


        $friday = $rooster->Openingstijden()->where('Dag','=','Friday')->first();
        if($request->friday_openingstijd && $request->friday_sluitingstijd){
            $friday->startTijd = $request->friday_openingstijd;
            $friday->eindTijd = $request->friday_sluitingstijd;
        }
        else{
            $friday->startTijd = "00:00:00";
            $friday->eindTijd = "00:00:00";   
        }
        $friday->save();

        $saturday = $rooster->Openingstijden()->where('Dag','=','Saturday')->first();
        var_dump($saturday);
        if($request->saturday_openingstijd && $request->saturday_sluitingstijd){
            $saturday->startTijd = $request->saturday_openingstijd;
            $saturday->eindTijd = $request->saturday_sluitingstijd;
        }
        else{
            $saturday->startTijd = "00:00:00";
            $saturday->eindTijd = "00:00:00";   
        }
        $saturday->save();
        
        $sunday = $rooster->Openingstijden()->where('Dag','=','Sunday')->first();
        if($request->sunday_openingstijd && $request->sunday_sluitingstijd){
            $sunday->startTijd = $request->sunday_openingstijd;
            $sunday->eindTijd = $request->sunday_sluitingstijd;
        }
        else{
            $sunday->startTijd = "00:00:00";
            $sunday->eindTijd = "00:00:00";
        }
        $sunday->save();

    }

    public function show($roosterId){
        return response()->json(["Rooster" => Rooster::find($roosterId), "Location" => "http://localhost:8000/api/location/" . Rooster::find($roosterId)->location()->first()->id]);
    }
    public function destroy($roosterId){
        $rooster = Rooster::find($roosterId);
        $openingstijden = $rooster->Openingstijden();
        $openingstijden->delete();
        $rooster->delete();
    }

    public function store(Request $request){
        $rooster = new Rooster();
        $rooster->name = $request->name;
        $location = Location::find($request->location_id);
        echo $location;
        $rooster->location()->associate($location);
        if($request->startDatum && $request->eindDatum){
            $rooster->startDatum = $request->startDatum;
            $rooster->eindDatum = $request->eindDatum;
        }
        else{
            $rooster->oneindig = $request->oneindig;
        }
        $rooster->aantalPersonen = $request->aantalPersonen;
        $rooster->duurTijdSlot = $request->duurTijdSlot;
        $rooster->duurTussenTijdSlot = $request->duurTussenTijdSlot;
        $rooster->save();
        
        $monday = new Openingstijden();
        $monday->rooster()->associate($rooster->id);
        $monday->Dag = "Monday";
        if($request->monday_openingstijd && $request->monday_sluitingstijd){
            $monday->startTijd = $request->monday_openingstijd;
            $monday->eindTijd = $request->monday_sluitingstijd;
        }
        else{
            $monday->startTijd = "00:00:00";
            $monday->eindTijd = "00:00:00";  
        }
        $monday->save();

        $tuesday = new Openingstijden();
        $tuesday->rooster()->associate($rooster->id);
        $tuesday->Dag = "Tuesday";
        if($request->tuesday_openingstijd && $request->tuesday_sluitingstijd){
            $tuesday->startTijd = $request->tuesday_openingstijd;
            $tuesday->eindTijd = $request->tuesday_sluitingstijd;
        }
        else{
            $tuesday->startTijd = "00:00:00";
            $tuesday->eindTijd = "00:00:00";  
        }
        $tuesday->save();

        $wednesday = new Openingstijden();
        $wednesday->rooster()->associate($rooster->id);
        $wednesday->Dag = "Wednesday";
        if($request->wednesday_openingstijd && $request->wednesday_sluitingstijd){
            $wednesday->startTijd = $request->wednesday_openingstijd;
            $wednesday->eindTijd = $request->wednesday_sluitingstijd;
        }
        else{
            $wednesday->startTijd = "00:00:00";
            $wednesday->eindTijd = "00:00:00";  
        }
        $wednesday->save();

        $thursday = new Openingstijden();
        $thursday->rooster()->associate($rooster->id);
        $thursday->Dag = "Thursday";
        if($request->thursday_openingstijd && $request->thursday_sluitingstijd){
            $thursday->startTijd = $request->thursday_openingstijd;
            $thursday->eindTijd = $request->thursday_sluitingstijd;
        }
        else{
            $thursday->startTijd = "00:00:00";
            $thursday->eindTijd = "00:00:00";     
        }
        $thursday->save();

        $friday = new Openingstijden();
        $friday->rooster()->associate($rooster->id);
        $friday->Dag = "Friday";
        if($request->friday_openingstijd && $request->friday_sluitingstijd){
            $friday->startTijd = $request->friday_openingstijd;
            $friday->eindTijd = $request->friday_sluitingstijd;
        }
        else{
            $friday->startTijd = "00:00:00";
            $friday->eindTijd = "00:00:00";   
        }
        $friday->save();

        $saturday = new Openingstijden();
        $saturday->rooster()->associate($rooster->id);
        $saturday->Dag = "Saturday";
        if($request->saturday_openingstijd && $request->saturday_sluitingstijd){
            $saturday->startTijd = $request->saturday_openingstijd;
            $saturday->eindTijd = $request->saturday_sluitingstijd;
        }
        else{
            $saturday->startTijd = "00:00:00";
            $saturday->eindTijd = "00:00:00";   
        }
        $saturday->save();
        
        $sunday = new Openingstijden();
        $sunday->rooster()->associate($rooster->id);
        $sunday->Dag = "Sunday";
        if($request->sunday_openingstijd && $request->sunday_sluitingstijd){
            $sunday->startTijd = $request->sunday_openingstijd;
            $sunday->eindTijd = $request->sunday_sluitingstijd;
        }
        else{
            $sunday->startTijd = "00:00:00";
            $sunday->eindTijd = "00:00:00";
        }
        $sunday->save();
    }

}
