<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maps;
use App\Location;

class MapsController extends Controller
{
    public function store(Request $request){
        if(Maps::where("location_id","=",$request->locationId)->count() === 0){
            $maps = new Maps();
            $location = Location::find($request->locationId);
            $maps->Location()->associate($location);
    
            $file = $request->map;
            $contents = $file->openFile()->fread($file->getSize());
    
            $maps->map = $contents;
            $maps->save();
        }
        else{
            $maps =  Maps::where("location_id","=",$request->locationId)->first();
            $location = Location::find($request->locationId);
            $maps->Location()->associate($location);
    
            $file = $request->map;
            $contents = $file->openFile()->fread($file->getSize());
    
            $maps->map = $contents;
            $maps->save();
        }
    }
    public function show($locationId){
        $map = Maps::where('location_id','=',$locationId)->first();

        return $map;
    }
}
