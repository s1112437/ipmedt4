<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bestellingen;
use App\BestellingOrders;

class BestellingOrdersController extends Controller
{
    public function getOrders($bestellingid){
        $order = Bestellingen::where('bestelling_id','=',$bestellingid)->get();
        return $order;
    }
    public function getByReservering($reserveringId){
        $bestellingen = BestellingOrders::where('reservering_id','=', $reserveringId)->get();
        return $bestellingen;
    }
    
    public function getAllOrders(){
        $orders = BestellingOrders::where('bestelling_id_order','!=',null)->get();
        return $orders;
    }
}
