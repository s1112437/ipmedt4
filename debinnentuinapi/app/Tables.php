<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    protected $table = 'table';
    public $timestamps = false;

    public function Location(){
        return $this->belongsTo('App\Location');
    }
    public function Reservering(){
        return $this->hasMany('App\Reserveringen');
    }
}
