<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Klanten extends Model
{
    protected $table = 'klanten';
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'phone',
    ];

    public function User() {
        return $this->belongsTo("App\User");
    }
    public function Reserveringen(){
        return $this->hasMany("App\Reserveringen");
    }
}
