<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserveringen extends Model
{
    protected $table = 'reservering';

    public function Tijdslot(){
        return $this->belongsTo('App\TijdSloten');
    }
    public function Table(){
        return $this->belongsTo('App\Tables');
    }
}
