<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Location;
use App\Rooster;
use App\Dagen;
use App\Openingstijden;
use App\TijdSloten;
use DateInterval;
use DatePeriod;
use DateTime;
class createTijdsloten extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createTijdsloten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command voor het creëren van de tijdsloten';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('UTC');
        $locaties = Location::all(); //Ophalen van alle locatie's
        $start_date = new DateTime(date('Y-m-d')); //Datum van vandaag ophalen
        $end_date = new DateTime(date('Y-m-d', strtotime("+7 days"))); //Datum van over 7 dagen ophalen
        $interval = DateInterval::createFromDateString('1 day'); //Interval creëren van 1 dag
        $end_date->modify('+1 day'); //Zorgen dat de 7e dag ook wordt meegenomen
        $period = new DatePeriod($start_date, $interval, $end_date); //Periode van week creëren
        foreach ($period as $day) { // Loopen door periode van 1 week
            $day = $day->format('Y-m-d'); //Datum van dag ophalen
            foreach($locaties as $locatie){ //Loopen door alle opgehaalde locatie's
                foreach($locatie->roosters as $rooster){ //Door roosters loopen
                    if(($rooster->startDatum <= $day && $rooster->eindDatum >= $day) || $rooster->oneindig){ //Controleren of dag in huidige rooster valt
                        if(Dagen::where('Datum',$day)->count() == 0){ //Controleren of dag nog niet bestaat
                            $dag = new Dagen; //Nieuwe dag creëren
                            $dag->Datum = $day; //Datum van dag zetten
                            $dag->save(); //Dag opslaan
                        }
                        else{ // Als dag wel bestaat
                            $dag = Dagen::find(Dagen::where('Datum',$day)->pluck('id'))->first(); //Haal bestaande dag op uit database
                        }
                            $openingstijd = $rooster->Openingstijden()->where('Dag', date('l', strtotime($day)))->get(); //Openingstijden van dag ophalen
                            $openingstijd = Openingstijden::find($openingstijd->pluck('id'))->first(); //Openingstijd ophalen
                            $startTime = new DateTime($openingstijd->startTijd); //Openingstijd ophalen
                            $endTime = new DateTime($openingstijd->eindTijd); //Sluitingstijd ophalen
                            $diffOpeningsTime = $startTime->diff($endTime, true)->h * 60 + $startTime->diff($endTime, true)->i; //Verschil tussen openingstijd en sluitingstijd berekenen en omzetten naar minuten
                            $duurTussenTijdSlot = date("H",  strtotime($rooster->duurTussenTijdSlot)) * 60 + date("i",  strtotime($rooster->duurTussenTijdSlot)); //Duur tussen 2 tijdsloten omzetten naar minuten
                            $timeSlot = date("H",  strtotime($rooster->duurTijdSlot)) * 60 + date("i",  strtotime($rooster->duurTijdSlot)) + $duurTussenTijdSlot; //Duur van tijdslot omzetten naar minuten
                            $amount = round($diffOpeningsTime / $timeSlot, 0); //Aantal tijdsloten berekenen
                            for($i = 0; $i < $amount; $i++){ //Loopen door aantal berekende tijdsloten
                                $tijdslot = new TijdSloten(); //Tijdslot aanmaken
                                if($i > 0){
                                    $tijdslot->startTijd = date('H:i:s', strtotime($openingstijd->startTijd) + (strtotime($rooster->duurTijdSlot) * $i + (strtotime($rooster->duurTussenTijdSlot)) * $i)); //Begintijd van tijdslot berekenen
                                }
                                else{
                                    $tijdslot->startTijd = date('H:i:s', strtotime($openingstijd->startTijd) + (strtotime($rooster->duurTijdSlot) * $i));
                                }
                                if(!$tijdslot->eindTijd =  date('H:i:s', strtotime($openingstijd->startTijd) + (strtotime($rooster->duurTijdSlot) * ($i + 1)) + (strtotime($rooster->duurTussenTijdSlot)) * $i) > date('H:i:s', strtotime($openingstijd->eindTijd))){ //Als eindtijd van tijdslot niet later ligt dan de sluitingstijd van het rooster
                                    
                                    $tijdslot->eindTijd =  date('H:i:s', strtotime($openingstijd->startTijd) + (strtotime($rooster->duurTijdSlot) * ($i + 1)) + (strtotime($rooster->duurTussenTijdSlot)) * $i); //Dan eindtijd installen op de starttijd van het rooster + de duur van de tijdslot * het aantal tijdsloten
                                }
                                else {
                                    $tijdslot->eindTijd = date('H:i:s',strtotime($openingstijd->eindTijd)); //Anders eindtijd van tijdslot instellen op eindtijd van rooster
                                }
                                $tijdslot->aantalPersonen = $rooster->aantalPersonen; //Aantal personen voor tijdslot ophalen uit rooster en aan tijdslot toewijzen
                                $tijdslot->beschikbaar = true; //Tijdslot op beschikbaar zetten
                                $tijdslot->Rooster()->associate($rooster); //Rooster aan tijdslot toewijzen
                                $tijdslot->Dagen()->associate($dag); //Dag aan tijdslot toewijzen
                                if(TijdSloten::where([['rooster_id',"=",$rooster->id],['dagen_id','=',$dag->id],['startTijd','=' ,$tijdslot->startTijd],['eindTijd', $tijdslot->eindTijd]])->count() == 0 ){
                                    $tijdslot->save(); //Tijdslot opslaan
                                }
                            };
                    }
                }
            }
        }

    }
}
