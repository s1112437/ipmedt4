<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TijdSloten extends Model
{
    protected $table = 'tijdslot';
    public $timestamps = false;

    public function Rooster(){
        return $this->belongsTo('App\Rooster');
    }
    public function Dagen(){
        return $this->belongsTo('App\Dagen');
    }
    public function Reserveringen(){
        return $this->hasMany('App\Reserveringen');
    }
}
