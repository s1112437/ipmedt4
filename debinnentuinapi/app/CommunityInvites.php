<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityInvites extends Model
{
    protected $table = "community_invites";
}
