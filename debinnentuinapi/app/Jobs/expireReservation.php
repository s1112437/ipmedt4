<?php

namespace App\Jobs;

use App\Reserveringen;
use App\TijdSloten;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class expireReservation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $reserveringId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($reserveringId)
    {
        $this->reserveringId = $reserveringId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $reservering = Reserveringen::find($this->reserveringId);
        if($reservering){
            $tijdslot = $reservering->Tijdslot()->first();
            $tijdslot->aantalPersonen += $reservering->aantalPersonen;
            $tijdslot->save();

            $reservering->delete();
            return 0;
        }
        return -1;
    }
}
