import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from "react-redux";
import { Provider } from "react-redux";
import { store } from "./Redux/Store";

const Header = (props) => {

    return (
        <Provider store={store}>
        <article id="js--header" className="header">
            <button onClick={() => props.history.back()} className="material-icons header__back">
            arrow_back_ios
            </button>
            <h1 className="header__title">
                {props.pageTitle}
            </h1>
            <section>
            </section>
        </article>
        </Provider>
    )
};
const mapStateToProps = state => {
    return {pageTitle: state.setHeaderTitle}
}
export default connect(mapStateToProps) (Header);