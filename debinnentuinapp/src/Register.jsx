import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import axios from "axios";

class Register extends React.Component{
    state = {user: {"name": '',"phone": '', "email": '', "password": '', "password_confirmation": ''}}

    
    componentDidMount(){

        this.props.setHeaderTitle("Register")
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log("JAAAAAAAA")

        let userData = this.state.user;
        console.log(this.state.user);
        if(this.props.match.params.token){
            axios.post("http://localhost:8000/api/auth/register/" + this.props.match.params.token, userData).then(response => {
                return response;
         
            }).then(json => {
                console.log(json)
                if (json.data.message){
                    console.log("Geregistreerd!")
                    this.props.history.push('/login')
                }
            })
        }
        axios.post("http://localhost:8000/api/auth/register", userData).then(response => {
            return response;
     
        }).then(json => {
            console.log(json)
            if (json.data.message){
                console.log("Geregistreerd!")
                this.props.history.push('/login')
            }
        })
    }
    handleClick = (path) =>{
        this.props.history.push(path)
    } 

    handleName = (e) => {
        let value = e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, name: value
            }
        }))
    }
    
    handlePhoneNumber = (e) => {
        let value = e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, phone: value
            }
        }))
    }
    
    handleEmail = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, email: value
            }
        })); 
    }

    handlePassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, password: value
            }
        })); 
    }
    handleConfirmPassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, password_confirmation: value
            }
        })); 
    }

    render(){
        return(
            <article className ="container register">

                {setHeaderTitle}
                <form onSubmit={this.handleSubmit} className="register__form">
                    
                    <label className="register__form__label" name="name">Naam</label>
                    <input className="register__form__input" onChange={this.handleName} type="text" placeholder="Vul naam in" required/>
                    <label className="register__form__label" name="phone">Telefoon nummer</label>
                    <input className="register__form__input" onChange={this.handlePhoneNumber} type="tel" placeholder="Vul telefoonnummer in" required/>
                    <label className="register__form__label"  name="email">E-mail</label>
                    <input className="register__form__input" onChange={this.handleEmail} type="email    " placeholder="Vul E-mail in" required/>
                    <label className="register__form__label"  name="password">Wachtwoord</label>
                    <input className="register__form__input" onChange={this.handlePassword} pattern=".{6,}" type="password" placeholder="Vul wachtworrd in" required/>
                    <label className="register__form__label" name="password">Controleer Wachtwoord</label>
                    <input className="register__form__input" onChange={this.handleConfirmPassword} type="password" placeholder="Herhaal wachtwoord" required/>     
                    <button className ="register__form__button" type="submit" value="Submit">Register</button>
                    <a onClick={() => this.handleClick("/login")} className="register__form__link">Terug naar login</a>

                </form>

            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle};
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle})(Register);