import React from 'react';
import {setHeaderTitle, setAuthenticated} from "./Redux/Actions";
import {connect} from "react-redux";
import AXIOS from "./axios";

class Bestelvalidatie extends React.Component {
    // TODO:
    // Fill in action in form fields
    // Update label input id's to create account/login/no account

    state = {
        showBestaandAccount: true,
        showNieuwAccount: false,
        showGeenAccount: false,
        isLoggedIn: false,
        error: '',
        existingUser: {
            email: '',
            password: ''
        },
        user: {
            "name": '',
            "phone": '',
            "email": '',
            "password": '',
            "password_confirmation": ''
        },
        geenUser: {
            geenUsername: '',
            geenUserphone: '',
            geenUseremail: ''
        }
    };

    componentDidMount() {
        this.props.setHeaderTitle("Bestelvalidatie")
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("JAAAAAAAA")

        let existingUserData = this.state.existingUser;
        // axios.defaults.headers.common['CSRF-Token'] = "my CSRF-Token"
        AXIOS.post("http://localhost:8000/api/auth/login", {
            email: this.state.existingUser.email,
            password: this.state.existingUser.password,

        }).then(response => {
            return response;

        }).then(json => {
            console.log(json);
            if (json.status === 200){
                AXIOS.defaults.headers.common['CSRF-Token'] = json.data.csrf_token;
                this.props.setAuthenticated(true);

                let reserveringKoppeling = {
                    klant_id: json.data.data.id,
                    reservering_id: this.props.reservering,
                    bestel_id: this.props.finalOrder['bestelling_id_order']
                };

                console.log(reserveringKoppeling);

                AXIOS.post("http://localhost:8000/api/koppelklantreservering", reserveringKoppeling)
                    .then(response => {
                        console.log(response)
                    }).then(error => {
                        console.log("Error message: " + error)
                });
                console.log("ingelogd en gekoppeld!")
                // this.handleClick('betaling')
            }
        }).catch(error => {if (error.response) {
            // The request was made and the server responded with a status code that falls out of the range of 2xx
            let err = error.response.data;
            this.setState({
                error: err.message,
                errorMessage: err.errors,
                formSubmitting: false
            })
        }
        else if (error.request) {
            // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
            let err = error.request;
            this.setState({
                error: err,
                formSubmitting: false
            })
        } else {
            // Something happened in setting up the request that triggered an Error
            let err = error.message;
            this.setState({
                error: err,
                formSubmitting: false
            })
        }
        }).finally(this.setState({error: ''}));
    };

    handleEmail = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            existingUser: {
                ...prevState.existingUser, email: value
            }
        }));
    };

    handlePassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            existingUser: {
                ...prevState.existingUser, password: value
            }
        }));
    };

    handleClick = (path) =>{
        this.props.history.push(path)
    };

    // New User
    handleNewSubmit = (e) => {
        e.preventDefault();

        let userData = this.state.user;
        AXIOS.post("http://localhost:8000/api/auth/register", userData).then(response => {
            return response;
        }).then(json => {
            if (json.data.message){
                let reserveringKoppeling = {
                    klant_id: json.data.klant,
                    reservering_id: this.props.reservering,
                    bestel_id: this.props.finalOrder['bestelling_id_order']
                };

                // Update reservering table, insert klant ID
                AXIOS.post('http://localhost:8000/api/koppelklantreservering', reserveringKoppeling)
                    .then(response => {
                        console.log(response)
                    }).then(error => {
                        console.log("Error message: " + error)
                });
                console.log("Geregistreerd!")
            }
        })
    };

    handleNewName = (e) => {
        let value = e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, name: value
            }
        }))
    };

    handleNewPhoneNumber = (e) => {
        let value = e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, phone: value
            }
        }))
    };

    handleNewEmail = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, email: value
            }
        }));
    };

    handleNewPassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, password: value
            }
        }));
    };

    handleConfirmPassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, password_confirmation: value
            }
        }));
    };

    // Geen account
    handleGeenAccountSubmit = (e) => {
        e.preventDefault();


        console.log("Gebruiker wil geen account hebben");
        let userData = this.state.geenUser;
        console.log(userData);
    };

    handleGeenName = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            geenUser: {
                ...prevState.geenUser, geenUsername: value
            }
        }));
    };

    handleGeenEmail = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            geenUser: {
                ...prevState.geenUser, geenUseremail: value
            }
        }));
    };

    handleGeenPhoneNumber = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            geenUser: {
                ...prevState.geenUser, geenUserphone: value
            }
        }));
    };

    // Update radio buttons
    bestaandAccount = () => {
        this.setState({
            showBestaandAccount: true,
            showNieuwAccount: false,
            showGeenAccount: false,
        })
    };

    nieuwAccount = () => {
        this.setState({
            showBestaandAccount: false,
            showNieuwAccount: true,
            showGeenAccount: false,
        })
    };

    geenAccount = () => {
        this.setState({
            showBestaandAccount: false,
            showNieuwAccount: false,
            showGeenAccount: true,
        })
    };

    render() {
        if (this.props.isAuthenticated) {
            this.handleClick('/betaling');
        }
        return(
            <article className={"container bestelvalidatie"}>
                {setHeaderTitle}
                <section className={"bestelvalidatie__radiobuttons"}>
                    <form className={"bestelvalidatie__radiobuttons__form"}>
                        <label><input type="radio" name="radio" onClick={this.bestaandAccount} defaultChecked={"true"}/> Inloggen in een bestaand account</label>
                        <label><input type="radio" name="radio" onClick={this.nieuwAccount} /> Nieuw account aanmaken</label>
                        <label><input type="radio" name="radio" onClick={this.geenAccount} /> Bestellen zonder account</label>
                    </form>
                </section>

                {this.state.showBestaandAccount && (
                    <form onSubmit={this.handleSubmit} className={"bestelvalidatie__form"}>
                        <div className={"bestelvalidatie__form__labels"}>
                            <label className={"bestelvalidatie__form__label"}>E-mail: <input onChange={this.handleEmail} className={"bestelvalidatie__form__input"} type="email" placeholder={"Vul hier uw e-mail adres in"} required/></label>
                            <label className={"bestelvalidatie__form__label"}>Wachtwoord: <input onChange={this.handlePassword} className={"bestelvalidatie__form__input"} type="password" placeholder={"Vul hier uw wachtwoord in"} required/></label>
                        </div>

                        <button type={"submit"} value={"Submit"} className={"bestelvalidatie__form__button"}>Login</button>
                        <a  onClick={() => this.handleClick("/wachtwoordHerstel")} className="login__form__link">Wachtwoord vergeten?</a>
                    </form>
                )}

                {this.state.showNieuwAccount && (
                    <form onSubmit={this.handleNewSubmit} className={"bestelvalidatie__form"}>
                        <div className={"bestelvalidatie__form__labels"}>
                            <label className={"bestelvalidatie__form__label"}>Naam: <input onChange={this.handleNewName} className={"bestelvalidatie__form__input"} type="text" placeholder={"Vul hier uw volledige naam in"} required/></label>
                            <label className={"bestelvalidatie__form__label"}>E-mail: <input onChange={this.handleNewEmail} className={"bestelvalidatie__form__input"} type="email" placeholder={"Vul hier uw e-mail adres in"} required/></label>
                            <label className={"bestelvalidatie__form__label"}>Telefoonnummer: <input onChange={this.handleNewPhoneNumber} className={"bestelvalidatie__form__input"} type="text" placeholder={"Vul hier uw telefoonnummer in"} required/></label>

                            <label className={"bestelvalidatie__form__label"}>Nieuw wachtwoord: <input onChange={this.handleNewPassword} className={"bestelvalidatie__form__input"} type="password" placeholder={"Vul hier uw wachtwoord in"} required/></label>
                            <label className={"bestelvalidatie__form__label"}>Herhaal wachtwoord: <input onChange={this.handleConfirmPassword} className={"bestelvalidatie__form__input"} type="password" placeholder={"Vul hier uw wachtwoord in"} required/></label>
                        </div>

                        <button className={"bestelvalidatie__form__button"}>Maak nieuw account</button>
                    </form>
                )}

                {this.state.showGeenAccount && (
                    <form onSubmit={this.handleGeenAccountSubmit} className={"bestelvalidatie__form"}>
                        <div className={"bestelvalidatie__form__labels"}>
                            <label className={"bestelvalidatie__form__label"}>Naam: <input onChange={this.handleGeenName} className={"bestelvalidatie__form__input"} type="text" placeholder={"Vul hier uw volledige naam in"} required/></label>
                            <label className={"bestelvalidatie__form__label"}>E-mail: <input onChange={this.handleGeenEmail} className={"bestelvalidatie__form__input"} type="email" placeholder={"Vul hier uw e-mail adres in"} required/></label>
                            <label className={"bestelvalidatie__form__label"}>Telefoonnummer: <input onChange={this.handleGeenPhoneNumber} className={"bestelvalidatie__form__input"} type="text" placeholder={"Vul hier uw telefoonnummer in"} required/></label>
                        </div>

                        <button className={"bestelvalidatie__form__button"}>Ga verder</button>
                    </form>
                )}
            </article>
        )
    }
};

const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle, isAuthenticated: state.isAuthenticated, reservering: state.reservering, location: state.location, finalOrder: state.finalOrder };
};

export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle, setAuthenticated: setAuthenticated}) (Bestelvalidatie);