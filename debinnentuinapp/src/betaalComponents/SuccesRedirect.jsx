import React from 'react';
class SuccesRedirect extends React.Component{


    render(){
        return (
          <section className="betaling__succes">
            <p className="betaling__succes__text">Klik <a className="betaling__succes__text__redirect" href="https://rubenjerry.nl/">hier</a> om terug te gaan</p>
          </section>
        );
    }

}

export default SuccesRedirect;
