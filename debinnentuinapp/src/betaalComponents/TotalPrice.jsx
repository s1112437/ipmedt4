import React from 'react';
import axios from 'axios';

class TotalPrice extends React.Component{

    state = {
      price: ""
    }

    getPrice= () => {
      if(this.state.price == ""){
        const BASE_URL = "http://localhost:8000/api/bestellingen/getprice";
        axios.get(BASE_URL).then(res=>{
            console.log(res.data);
            const double = parseFloat(res.data).toFixed(2);
            console.log(double);
            this.setState({price: double})
        });
      }
    }

    render(){
        return (
          <section className="betaling__price">
           <p className="betaling__price__text" id="js--price">&euro; {this.getPrice()}
           {this.state.price}</p>
          </section>
        );
    }

}

export default TotalPrice;
