import React from 'react';
import { connect } from 'react-redux';


export default class Location extends React.Component {
    selectLocation = () => {
        this.props.selectLocation(this.props.locationId);
    }
    render(){
    return (
        <button onClick={this.selectLocation} className={this.props.styling}>
            {this.props.locationName}
        </button>
    )
    }
}
