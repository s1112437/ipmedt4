import React from 'react';

export default class ReservateButton extends React.Component {
    makeReservation = (e) =>{
        e.preventDefault()
        this.props.makeReservation(this.props.selectedTimeslot, this.props.aantalPersonen);
    }
    render(){
        console.log(this.props.stylingButton)
        return (
            <article className="reserveren__reservateButtonContainer">
            <section className="reserveren__reservateButtonContainer__message">{this.props.msg}</section>
            <button onClick={this.makeReservation} className={this.props.stylingButton}>
                Reserveren
                <small className="reserveren__reservateButtonContainer__reservateButton__subtitle">Ga door naar bestellen</small>
            </button>
            </article>

        )
    }
}