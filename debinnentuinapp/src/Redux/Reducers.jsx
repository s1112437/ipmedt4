//After an action object is created it goes to all the reducers.
//1. Check if type compatible
//2. do something with payload
//3. return stage

import {SET_LOCATION, SET_AUTHENTICATED, SET_RESERVERING, SET_HEADER_TITLE, SET_CURRENT_ROOSTER, SET_MESSAGE, SET_MODAL_DISPLAY_ON, SET_MODAL_DISPLAY_OFF, 
    GET_CATEGORY_MENU_DATA, SET_TEXT_AREA_CONTENT, STORE_FINAL_ORDERS, SET_MENU_OBJECT_QUANTITY, SET_CART_DISPLAY_STYLE, SET_CART_AMOUNT, SET_CURRENT_ORDER_OM, SET_CUSTOMER, SET_ADMIN_AUTHENTICATED} from "./Actions";

import update from 'immutability-helper';
// npm install immutability-helper --save


export const setHeaderTitle = (state = 0, action) => {
    switch(action.type){
        case SET_HEADER_TITLE:
            return action.payload;
        default:
            return state;
    }
};

export const currentRooster = (state = "", action) => {
    switch(action.type){
        case SET_CURRENT_ROOSTER:
            return action.payload;
        default:
            return state;
    }
}

export const message = (state = "", action) => {
    switch(action.type){
        case SET_MESSAGE:
            return action.payload;
        default:
            return state;
    }
}
export const reservering = (state = "", action) => {
    switch(action.type){
        case SET_RESERVERING:
            return action.payload;
    default:
        return state;
    }
}
export const isAuthenticated = (state = false, action) => {
    switch(action.type){
        case SET_AUTHENTICATED:
            return action.payload;
        default:
            return state;
    }
}
export const isAdminAuthenticated = (state = false, action) => {
    switch(action.type){
        case SET_ADMIN_AUTHENTICATED:
            return action.payload;
        default:
            return state;
    }
}
export const location = (state = 1, action) => {
    switch(action.type){
        case SET_LOCATION:
            return action.payload;
    default:
        return state;
    }
}

export const modal_display = (state = false, action)=>{
    switch(action.type){
        case SET_MODAL_DISPLAY_ON:
            return action.payload;
        case SET_MODAL_DISPLAY_OFF:
            return action.payload;
        default:
            return state;
    }
}

export const menu_data = (state = "", action)=>{
    switch(action.type){
        case GET_CATEGORY_MENU_DATA:
            return action.payload;
        default:
            return state;
    }
}

export const object_quantity= (state = [], action) =>{
    switch(action.type){
        case SET_MENU_OBJECT_QUANTITY:
            for(let i = 0; i<state.length;i++){
                if(action.payload["menu_id"] === state[i]["menu_id"]){
                    return update(state,{
                        [i]:{
                            menu_id: {$set: action.payload["menu_id"]},
                            quantity: {$set: action.payload["quantity"]}
                        }
                    });
                }
            }
            return [...state,action.payload];

        default: 
            return state;
    }  
};

export const cartItem = (state = [], action)=>{
    switch(action.type){
        case SET_MENU_OBJECT_QUANTITY:
            for(let i = 0; i<state.length;i++){
                if(action.payload["menu_item"] === state[i]["menu_item"] & action.payload["quantity"] === 0){
                    return state.filter( item=> item != state[i]  )
                }

                else if(action.payload["menu_id"] === state[i]["menu_id"]){
                    return update(state,{
                        [i]:{
                            menu_id: {$set: action.payload["menu_id"]},
                            quantity: {$set: action.payload["quantity"]}
                        }
                    });
                }
            }
            return [...state,action.payload];

        default: 
            return state;
    }  
}

export const finalOrder = (state = "", action)=>{
    switch(action.type){
        case STORE_FINAL_ORDERS:
            return action.payload;
        default:
            return state;
    }
}

export const cartDisplay = (state="none", action)=>{
    switch(action.type){
        case SET_CART_DISPLAY_STYLE:
            return action.payload;
        default:
            return state;
    }
}

export const cartAmount = (state=0, action)=>{
    switch(action.type){
        case SET_CART_AMOUNT:
            return action.payload;
        default:
            return state;
    }
}
export const textArea = (state="", action)=>{
    switch(action.type){
        case SET_TEXT_AREA_CONTENT:
            return action.payload;
        default:
            return state;
    }
}

export const customer = (state = null, action)=>{
    switch(action.type){
        case SET_CUSTOMER:
            return action.payload;
        default:
            return state;
    }
}
export const current_order_om = (state="", action)=>{
    switch(action.type){
        case SET_CURRENT_ORDER_OM:
            return action.payload;
        default:
            return state;
    }
}