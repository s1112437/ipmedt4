//here are the action creators. There responsibility is to create an action object
export const SET_HEADER_TITLE = "SET_HEADER_TITLE";
export const SET_CURRENT_ROOSTER = "SET_CURRENT_ROOSTER";
export const SET_MESSAGE = "SET_MESSAGE";
export const SET_RESERVERING = "SET_RESERVERING";
export const SET_LOCATION = "SET_LOCATION";
export const SET_AUTHENTICATED = "SET_AUTHENTICATED";
export const SET_ADMIN_AUTHENTICATED = "SET_ADMIN_AUTHENTICATED";
export const SET_MODAL_DISPLAY_ON = "SET_MODAL_DISPLAY_ON";
export const SET_MODAL_DISPLAY_OFF = "SET_MODAL_DISPLAY_OFF";

export const GET_CATEGORY_MENU_DATA = "GET_CATEGORY_MENU_DATA";

export const SET_MENU_OBJECT_QUANTITY= "SET_MENU_OBJECT_QUANTITY";


export const SET_CART_ITEM_ORDERS= "SET_CART_ITEM_ORDERS";

export const STORE_FINAL_ORDERS= "STORE_FINAL_ORDERS";

export const SET_CART_DISPLAY_STYLE = "SET_CART_DISPLAY_STYLE";

export const SET_CART_AMOUNT = "SET_CART_AMOUNT";

export const SET_TEXT_AREA_CONTENT = "SET_TEXT_AREA_CONTENT";
export const SET_CUSTOMER = "SET_CUSTOMER";


export const SET_CURRENT_ORDER_OM = "SET_CURRENT_ORDER_OM";
export const setHeaderTitle = title => ({
    type: SET_HEADER_TITLE,
    payload: title
})

export const setCurrentRooster = roosterId => ({
    type: SET_CURRENT_ROOSTER,
    payload: roosterId
})

export const setMessage = (typeMsg, message) => ({
    type: SET_MESSAGE,
    payload: {
        Type: typeMsg,
        Message: message
    }
})

export const setReservering = reserveringId => ({
    type: SET_RESERVERING,
    payload: reserveringId
})

export const setLocation = locationId => ({
    type: SET_LOCATION,
    payload: locationId
})
export const setAuthenticated = isAuthenticated => ({
    type: SET_AUTHENTICATED,
    payload: isAuthenticated
})
export const setAdminAuthenticated = isAdminAuthenticated => ({
    type: SET_ADMIN_AUTHENTICATED,
    payload: isAdminAuthenticated
})
export const setModalDisplayOn = () =>({
    type: SET_MODAL_DISPLAY_ON,
    payload: true
    
})
export const setModalDisplayOff = () =>({
    type: SET_MODAL_DISPLAY_OFF,
    payload: false
})

export const getCategoryMenuData = ( menu_data ) =>({
    type: GET_CATEGORY_MENU_DATA,
    payload: menu_data
})


export const setMenuObjectQuantity = object_quantity => ({
    type: SET_MENU_OBJECT_QUANTITY,
    payload: object_quantity
})

export const setCartItemOrders = ( cartItem ) => ({
    type: SET_CART_ITEM_ORDERS,
    payload: cartItem
})

export const storeFinalOrders = ( finalOrder ) =>({
    type: STORE_FINAL_ORDERS,
    payload: finalOrder

})
export const setCartDisplayStyle = ( cartDisplay ) =>({
    type: SET_CART_DISPLAY_STYLE,
    payload: cartDisplay
})

export const setCartAmount = ( cartAmount ) =>({
    type: SET_CART_AMOUNT,
    payload: cartAmount
})

export const setTextAreaContent = ( textContent ) =>({
    type: SET_TEXT_AREA_CONTENT,
    payload: textContent
})
export const setCustomer = (customerId) =>({
    type: SET_CUSTOMER,
    payload: customerId  
})
export const setCurrentOrderOM = ( current_order_om ) =>({
    type: SET_CURRENT_ORDER_OM,
    payload: current_order_om
})
