import {combineReducers, createStore} from "redux";
import {setHeaderTitle, currentRooster, message, reservering, location, isAuthenticated,isAdminAuthenticated, modal_display, menu_data, cartItem, finalOrder, object_quantity, 
    cartDisplay, cartAmount, textArea, current_order_om, customer} from "./Reducers";

export const store = createStore(
    combineReducers({
        setHeaderTitle, currentRooster, message, reservering, location,  isAuthenticated, isAdminAuthenticated, modal_display, menu_data, cartItem, finalOrder, object_quantity, 
        cartDisplay, cartAmount, textArea, current_order_om, customer
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
