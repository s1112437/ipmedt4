import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import axios from "axios";


class WachtwoordHerstel extends React.Component{

    state = {email: null, succesmessage: null, errormessage: null};

    onChange = (e) => {
        this.setState({
            email: e.target.value
        });
    }

    componentDidMount(){
        this.props.setHeaderTitle("wachtwoord Herstel")
    }
    
    handleClick = (path) =>{
        this.props.history.push(path)
    } 

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("JAAAAAAAA");
        console.log(this.state.email);
        axios.post("http://localhost:8000/api/send-mail", {
            email: this.state.email
           
            }).then((res)=>{
                console.log(res)
                if(!res.data.error){
                    this.setState({
                        succesmessage: res.data.message
                    
                    })
                }else{
                    this.setState({
                        errormessage: res.data.message
                    })
                }
            })
        }

    render(){

        return(
            <article className ="container wachtwoordHerstel">
                {setHeaderTitle}
                <form className="wachtwoordHerstel__form" onSubmit={this.handleSubmit}>         
                    <p>{this.state.succesmessage}</p>  
                    <p>{this.state.errormessage}</p>          
                    <input onChange={this.onChange} className="wachtwoordHerstel__form__input" type="text" placeholder="Vul E-mail in" required/>
   
                    <button  className ="wachtwoordHerstel__form__button" type="submit" value="Submit">Stuur wachtwoord reset link</button>
                    <a  onClick={() => this.handleClick("/login")} className="wachtwoordHerstel__form__link">Terug naar login</a>
                </form>

            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle, userGegevens: state.userGegevens };
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle}) (WachtwoordHerstel);