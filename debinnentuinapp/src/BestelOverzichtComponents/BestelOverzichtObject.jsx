import React from "react";
import { setMenuObjectQuantity, setCartAmount} from "../Redux/Actions";
import { connect } from 'react-redux';

class BestelOverzichtObject extends React.Component{
    IncrementItem = () => {
        let id = this.props.menu_item_id;
        let menuArrayData = this.props.cartItem;
        if(menuArrayData.length > 0){
            for(let i = 0; i<menuArrayData.length; i++){
                if(menuArrayData[i]["menu_id"] === id & menuArrayData[i]["quantity"] < 9){
                    this.props.setMenuObjectQuantity({
                        menu_id: id,
                        menu_item: this.props.title,
                        menu_item_prijs: this.props.prize,
                        quantity: menuArrayData[i]["quantity"] + 1
                    }); return;
                }
            }
            this.props.setMenuObjectQuantity({
                menu_id: id,
                menu_item: this.props.title,
                menu_item_prijs: this.props.prize,
                quantity: 1
            }); return;
        }
        else{
            this.props.setMenuObjectQuantity({
                menu_id: id,
                menu_item: this.props.title,
                menu_item_prijs: this.props.prize,
                quantity: 1
            });
        }
    }

    DecreaseItem = () => {
        let id = this.props.menu_item_id;
        let menuArrayData = this.props.cartItem;
        if(menuArrayData.length > 0){
            for(let i = 0; i<menuArrayData.length; i++){
                if(menuArrayData[i]["menu_id"] === id & menuArrayData[i]["quantity"] === 1){
                    this.props.setMenuObjectQuantity({
                        menu_id: id,
                        menu_item: this.props.title,
                        menu_item_prijs: this.props.prize,
                        quantity: menuArrayData[i]["quantity"] - 1
                    });
                    this.props.setCartAmount(this.props.cartItem.length - 1);
                    return;
                }
                
                else if(menuArrayData[i]["menu_id"] === id & menuArrayData[i]["quantity"] !== 0){
                    this.props.setMenuObjectQuantity({
                        menu_id: id,
                        menu_item: this.props.title,
                        menu_item_prijs: this.props.prize,
                        quantity: menuArrayData[i]["quantity"] - 1
                    });
                }
                
            }
        }
    }

    setObjectQuantity = (data, menu_id) =>{
        if(data !==[]){
            for(let i=0; i<data.length; i++){
                if(data[i]["menu_id"] === menu_id){
                    return data[i]["quantity"];
                }
            }
        }
        return 0;
    }

    setPrizeToDouble = ()=>{
        return this.props.prize.toFixed(2);
    }
    

    render(){
        return(
            <section className="modal__bestellen__item">
                <div className="modal__bestellen__item__container1">
                    <h2 className="modal__bestellen__item__container1__itemtitle">{this.props.title}</h2>
                     <p className="modal__bestellen__item__container1__itemdescription">{this.props.description}</p>
                </div>

                <div className="modal__bestellen__item__container2">
                    <p className="modal__bestellen__item__container2__itemprize">{this.setPrizeToDouble()} &euro;</p>
                </div>
                
                
                <div className="modal__bestellen__item__container3 button_item">
                    <input type="button" value="-" onClick = {this.DecreaseItem} className="minus"/>

                    <input readOnly type="number" name="quantity" value={this.setObjectQuantity(this.props.object_quantity,this.props.menu_item_id)} className="input-number qtyicon"/>
                    
                    <input type="button" value="+" onClick = {this.IncrementItem} className="plus" />
                </div>
            </section>
        );
    }
}

const mapStateToProps = state =>{
    return { object_quantity: state.object_quantity, finalOrder: state.finalOrder, cartItem: state.cartItem};
}

export default connect(
    mapStateToProps,
    { setMenuObjectQuantity: setMenuObjectQuantity, setCartAmount:setCartAmount}
)(BestelOverzichtObject);
