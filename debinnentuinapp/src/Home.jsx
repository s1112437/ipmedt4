import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle, setAuthenticated} from "./Redux/Actions";
import  history from "./History";
import {
    Redirect
  } from "react-router-dom";
import axios from 'axios';

class Home extends React.Component{

    componentDidMount(){
        if(document.getElementById("js--container")){
            document.getElementById("js--container").style.display = "none";
            document.getElementById("js--header").style.display = "none";
        }
        if(!this.props.customer){
            document.getElementById('js--reserveringsBtn').style.display = "flex";
            document.getElementById('js--afhalenBtn').style.display = "flex";
        }
        // console.log(this.props);


    }
    
    handleClick = (path) => {
        document.location = path
    } 

    
    

    render(){
        console.log(this.props.isAuthenticated)
        if(this.props.customer){
            axios.get("http://localhost:8000/api/klantReserveringen/" + this.props.customer.id).then(res => {
            if(res.data.length == 0){
                document.getElementById('js--bijbestellenBtn').style.display = "none";
                document.getElementById('js--reserveringsBtn').style.display = "flex";
                document.getElementById('js--afhalenBtn').style.display = "flex";
            }
            if(res.data.length > 0){
                document.getElementById('js--bijbestellenBtn').style.display = "flex";
                document.getElementById('js--reserveringsBtn').style.display = "none";
                document.getElementById('js--afhalenBtn').style.display = "none";
            }
            })
        }


        return(
            <article className="home">
                <header className="home__header">
                    <img src="https://rubenjerry.nl/wp-content/uploads/2019/05/logo_green-300x67.png"/>
                    {this.props.isAuthenticated ? <button onClick={() => this.handleClick("/profiel")} className="home__header__link--profiel material-icons">person</button> : <button onClick={() => this.handleClick("/login")} className="home__header__link">Login</button>}
                    
                    
                </header>
                <article className="home__container">
                    <section className="home__container__grid">
                        <button id="js--afhalenBtn" onClick={() => this.handleClick("/bestellen")} 
                        className="home__container__grid__button home__container__grid__button--afhalen">Afhalen
                        <div className="home__container__grid__button__icons material-icons">arrow_forward</div></button>
                    
                        <button id="js--reserveringsBtn" onClick={() => this.handleClick("/reserveren")} 
                        className="home__container__grid__button home__container__grid__button--reserveren">Reserveren
                        <div className="home__container__grid__button__icons material-icons">arrow_forward</div></button>
                        
                        <button id="js--bijbestellenBtn" onClick={() => this.handleClick("/bestellen")} 
                        className="home__container__grid__button home__container__grid__button--bijbestellen">Bijbestellen
                        <div className="home__container__grid__button__icons material-icons">arrow_forward</div></button>
                    </section>
                </article>
            </article>
        )
    }
}
const mapStateToProps = state => {
    return {customer: state.customer, isAuthenticated: state.isAuthenticated}
}
export default connect(mapStateToProps, {setAuthenticated: setAuthenticated}) (Home);