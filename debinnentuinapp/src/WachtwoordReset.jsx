import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import axios from "axios";

class WachtwoordReset extends React.Component{
    state = {newPassword: null, confirmNewPassword: null, hideErrorMsg: true, hideForm: false, message: null}
    componentDidMount(){
        this.props.setHeaderTitle("Wachtwoord Reset")
        this.checkValid(this.props.match.params.token);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("Test")
        axios.post("http://localhost:8000/api/resetWachtwoord", {
            token: this.props.match.params.token,
            newPassword: this.state.newPassword,
            confirmNewPassword: this.state.confirmNewPassword
        }).then(response => {
            return response;
    });
}
    checkValid = (token) => {
        axios.get("http://localhost:8001/api/checkvalid/" + token).then((res) => {
            if(res.data.error){
                this.setState({
                    hideErrorMsg: false,
                    hideForm: true,
                    message: res.data.message
                })
            }
        })
    }
    handleClick = (path) =>{
        this.props.history.push(path)
    } 

    changeNewPassword = (e) => {
        this.setState({
            newPassword: e.target.value
        });
    }
    changeConfirmNewPassword = (e) => {
        this.setState({
            confirmNewPassword: e.target.value
        });
    }

    render(){
        return(
            <article className="container wachtwoordReset">
                {setHeaderTitle}
                <form  className={this.state.hideForm ? "wachtwoordReset__form wachtwoordReset__form--hide" : "wachtwoordReset__form"} onSubmit={this.handleSubmit}>           
                    <label className="wachtwoordReset__form__label" type="text">Nieuw wachtwoord</label>         
                    <input className="wachtwoordReset__form__input" onChange={this.changeNewPassword} type="password" placeholder="Voer nieuw wachtwoord in" required/>
                    <label className="wachtwoordReset__form__label" type="text">Bevestig wachtwoord</label>   
                    <input className="wachtwoordReset__form__input" onChange={this.changeConfirmNewPassword} type="password" placeholder="Bevestig wachtwoord" required/>
                    <button className="wachtwoordReset__form__button" type="submit" value="Submit">Reset wachtwoord</button>
                </form>
  
                <section className={this.state.hideErrorMsg ? "wachtwoordReset__section wachtwoordReset__section--hide" : "wachtwoordReset__section"}>
                    <p>{this.state.message} </p>
                </section>

            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle };
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle}) (WachtwoordReset);