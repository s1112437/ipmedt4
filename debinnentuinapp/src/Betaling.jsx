import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import TotalPrice from "./betaalComponents/TotalPrice";
import OrderId from "./betaalComponents/OrderId";
import BetaalButton from "./betaalComponents/BetaalButton";
import PayLogo from "./betaalComponents/PayLogo";

class Betaling extends React.Component{
   componentDidMount(){
        this.props.setHeaderTitle("Betaling")
    }

   render(){
      return(
        <article className="container betaling">
          <PayLogo />
          <OrderId />
          <TotalPrice />
          <BetaalButton />
        </article>
      )
   }

}

const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle };
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle}) (Betaling);
