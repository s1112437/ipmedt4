import Modal from 'react-modal';
// npm install react-modal

import React from 'react';

import BestellenModalObject from './BestellenModalObject';
import BestellenModalAddToCartButton from './BestellenModalAddToCartButton';

import { connect } from 'react-redux';
import { setModalDisplayOff } from "../Redux/Actions";

Modal.setAppElement('body');

class BestellenModal extends React.Component{
    closeModal = () =>{
        this.props.setModalDisplayOff();
    }

    displayMainCategoryTitle = (data) =>{
        if(data !== ""){
            if(data[0].categorie == "theroof"){
                return "The Roof";
            }
            return data[0].categorie;
        }
    }

    displayContent = (data)=>{
        if(data !== ""){
            let list = []
            for(let i = 0; i<data.length;i++){
                list.push(
                <BestellenModalObject 
                    title={data[i].menu_item}
                    menu_item_id={data[i].menu_item_id}
                    description={data[i].description}
                    prize={data[i].prijs}
                    key={data[i].menu_item_id}
                    />
                )
            }
            return list;
        }
    }
    
    render(){
        return(
            <article>
                <Modal className="modal__bestellen" overlayClassName="modal__bestellen__overlay" isOpen={this.props.modal_display} onRequestClose={()=> this.closeModal()}>
                    
                    <button className= "modal__bestellen__close" onClick={this.closeModal}>x</button>
                    
                    <h1 className="modal__bestellen__title">{this.displayMainCategoryTitle(this.props.menu_data)}</h1>

                    <section className="modal__bestellen__subcontainer"> 
                        {this.displayContent(this.props.menu_data)}
                    </section>
                    
                    <BestellenModalAddToCartButton/>
                   
                </Modal>
                
            </article>
        )
    }
}

const mapStateToProps = state =>{
    return { 
        modal_display: state.modal_display, 
        menu_data: state.menu_data
    };
}

export default connect(
    mapStateToProps,
    {setModalDisplayOff: setModalDisplayOff}
)(BestellenModal);