import React from 'react';
import {connect} from "react-redux";
import { setCartDisplayStyle } from "../Redux/Actions";

class BestellenMainHeader extends React.Component{
    onCartClicked = () =>{
        if(this.props.cartDisplayStatus === "block"){
            this.props.setCartDisplayStyle("none");
        }
        else{
            this.props.setCartDisplayStyle("block");
        }
    }

    render(){
        return (
            <article className="header">
            <button onClick={() => this.props.history.back()} className="material-icons header__back">
            arrow_back_ios
            </button>
                
                <h1 className="header__title">{this.props.pageTitle}</h1>

                <figure onClick={this.onCartClicked} className="material-icons header__cart">
                    <img className="header__cart__image"src="https://cdn.iconscout.com/icon/free/png-256/shopping-cart-452-1163339.png"/>
                    <i className="header__cart__imageindicator">{this.props.cartItemsAmount}</i>
                </figure>
            </article>
        )
    }
};
const mapStateToProps = state => {
    return { pageTitle: state.setHeaderTitle, cartDisplayStatus: state.cartDisplay, cartItemsAmount: state.cartAmount}
}
export default connect(mapStateToProps,
    {setCartDisplayStyle:setCartDisplayStyle})(BestellenMainHeader);