import React from 'react';
import { connect } from 'react-redux';
import { setCartDisplayStyle } from "../Redux/Actions";

import BestellenCartOverzichtObject from './BestellenCartOverzichtObject';

class BestellenCartOverzicht extends React.Component{
    displayCartOverzicht = (data) =>{
        if(data !== ""){
            let list = []
            for(let i = 0; i<data.length;i++){
                list.push(
                <BestellenCartOverzichtObject
                    title={data[i].menu_item}
                    menu_item_id={data[i].menu_id}
                    prize={data[i].menu_item_prijs}
                    menu_quantity={data[i].quantity}
                    key={data[i].menu_id}
                    />
                )
                if(data[i].quantity === 0){
                    list.pop(i)
                }
            }
            return list;
        }
    }

    setCartDisplayStyle = ( cartDisplayState ) =>{
        if(cartDisplayState === 'block'){
            return {display: 'block'};
        }
        else{
            return {display: 'none'};
        }
    }

    getTotalPrize = (arrayCart) =>{
        let totaal = 0;
        if(arrayCart !== []){
            for(let i = 0; i<arrayCart.length;i++){
                totaal += arrayCart[i]['quantity'] * arrayCart[i]['menu_item_prijs']
            }
            return totaal.toFixed(2);
        }
        return 0;
    }
    
    render(){
        return(
            <section style={this.setCartDisplayStyle(this.props.cartDisplay)} className="bestellen__cart__overzicht">
                <section className="bestellen__cart__container">
                    {this.displayCartOverzicht(this.props.cartItem)}
                </section>
        
                <h2 className="bestellen__cart__overzicht__totaal">Totaal: {this.getTotalPrize(this.props.cartItem)} &euro;</h2>
            </section>
            
        );
    }
}

const mapStateToProps = state =>{
    return { cartItem: state.cartItem, cartDisplay: state.cartDisplay };
}
export default connect(
    mapStateToProps,
    { setCartDisplayStyle: setCartDisplayStyle }
)(BestellenCartOverzicht);