import React from 'react';
import MenuTheRoofGrid from './MenuTheRoofGrid';
import axios from 'axios';
import { getCategoryMenuData } from "../Redux/Actions";
import { connect } from 'react-redux';
class MenuTheRoof extends React.Component{
    componentDidMount = () =>{
        axios.get("http://localhost:8000/api/bestellen/theroof").then(res=>{
            this.props.getCategoryMenuData(res.data);
        });
    }
    
    render(){
        
        return(
            <article className="container bestellen__theroof">
                <section className="bestellen__theroof__c1">
                    <p className="bestellen__theroof__c1__paragraph">*Uitleg over the roof*<br></br>*Uitleg over de strippen*<br></br>*Uitleg over entree*</p>
                </section>
                
                <MenuTheRoofGrid />
                
            </article>
        );
    }
}
const mapStateToProps = state =>{
    return { menu_data: state.menu_data };
}
export default connect(
    mapStateToProps,
    { getCategoryMenuData: getCategoryMenuData }
)(MenuTheRoof);
