import React from 'react';
import { connect } from 'react-redux';
import { setCartItemOrders, storeFinalOrders, setCartAmount} from "../Redux/Actions";

class BestellenModalAddToCartButton extends React.Component{
    addToCart = () =>{
        for(let i = 0; i<this.props.object_quantity.length; i++){
            this.props.setCartItemOrders(this.props.object_quantity[i]);
        }
        this.props.setCartAmount(this.props.cartItem.length);
        console.log(this.props.cartItem);
    }
    
    render(){
        return(
            <section className="container bottom">
                <button className="--bevestig_bestelling"onClick={this.addToCart}>
                    <h4>Voeg aan winkelwagen</h4>
                </button>
            </section>
        );
    }
}

const mapStateToProps = state =>{
    return { cartItem: state.cartItem, object_quantity: state.object_quantity };
}

export default connect(
    mapStateToProps,
    {   setCartItemOrders: setCartItemOrders,
        storeFinalOrders:storeFinalOrders,
        setCartAmount :setCartAmount }  
)(BestellenModalAddToCartButton);