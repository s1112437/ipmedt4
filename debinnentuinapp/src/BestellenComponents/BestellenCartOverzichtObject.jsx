import React from 'react';
import { connect } from 'react-redux';
class BestellenCartOverzichtObject extends React.Component{
    calculateTotalPrize = (quantity, prize) =>{
        return (quantity * prize).toFixed(2);
    }
    render(){
        return(
            <section className="bestellen__cart__item">
                <div className="bestellen__cart__item__c1">
                    <h2 className="bestellen__cart__item__c1__itemtitle">{this.props.title}</h2>
                </div>
                <div className="bestellen__cart__item__c2">
                    <p className="bestellen__cart__item__c2__itemquantity">{this.props.menu_quantity}x</p>
                </div>
                <div className="bestellen__cart__item__c3">
                    <p className="bestellen__cart__item__c3__itemprize">{this.calculateTotalPrize(this.props.menu_quantity,this.props.prize)} &euro;</p>
                </div>
            </section>
        )
    }
}

const mapStateToProps = state =>{
    return { cartItem: state.cartItem };
}

export default connect(mapStateToProps)(BestellenCartOverzichtObject);
