import React from 'react';

import MenuTheRoofGridItem from './MenuTheRoofGridItem';
import { getCategoryMenuData} from "../Redux/Actions";
import { connect } from 'react-redux';

class MenuTheRoofGrid extends React.Component{
    displayOverzichtContent = (data)=>{
        let list = []
        console.log(data);
        if(data !== null){
            for(let i = 0; i<data.length;i++){
                list.push(
                <MenuTheRoofGridItem
                    title={data[i].menu_item}
                    menu_item_id={data[i].menu_item_id}
                    description={data[i].description}
                    prize={data[i].prijs}
                    key={data[i].menu_item_id}
                    />
                )
            }
        }
        return list;
    }
    
    render(){
        return(
            <section className="bestellen__theroof__c2">
                {this.displayOverzichtContent(this.props.menu_data)}
            </section>
        );
    }
}
const mapStateToProps = state =>{
    return { 
        menu_data: state.menu_data
    };
}
export default connect(
    mapStateToProps,
    { getCategoryMenuData :getCategoryMenuData,}
)(MenuTheRoofGrid);
