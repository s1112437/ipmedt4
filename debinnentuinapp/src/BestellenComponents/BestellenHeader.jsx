import React from 'react';

class BestellenHeader extends React.Component{
    render(){
        return (
            <header className="bestellen__header">
                <section className="bestellen__header__title">
                    15% korting op al onze producten voor de PLNT community & studenten
                </section>
            </header>
        );
    }
}

export default BestellenHeader;