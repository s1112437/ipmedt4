import React from 'react';
import MenuCategorie from './MenuCategorie';

class MenuCategorieList extends React.Component{
    render(){
        return(
            <section className="container bestellen--container_categories">
                <MenuCategorie 
                    title="Snacks" 
                    id="snacks"/>

                <MenuCategorie 
                    title="Toasts" 
                    id="toasts"/>

                <MenuCategorie 
                    title="Salads" 
                    id="salads"/>

                <MenuCategorie 
                    title="Sandwiches" 
                    id="sandwiches"/>

                <MenuCategorie 
                    title="Coffee" 
                    id="coffee"/>

                <MenuCategorie 
                    title="Hot Drinks" 
                    id="hot_drinks"/>

                <MenuCategorie 
                    title="Cold Drinks" 
                    id="cold_drinks"/>
            </section>
        );
    }
}
export default MenuCategorieList;
