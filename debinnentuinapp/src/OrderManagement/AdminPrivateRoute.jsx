import React from "react";
import {
  Route,
  Redirect
} from "react-router-dom";
import { connect } from 'react-redux'; 

const AdminPrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    rest.isAdminAuthenticated ? (
      <Component {...props} />
    ) : (
      <Redirect to = {{
        pathname: '/admin/login',
        state: { from: props.location}
      }} />
    )
    )} />
)
const mapStateToProps = (state) => {
    return { isAdminAuthenticated: state.isAdminAuthenticated};
}
export default connect(mapStateToProps) (AdminPrivateRoute);