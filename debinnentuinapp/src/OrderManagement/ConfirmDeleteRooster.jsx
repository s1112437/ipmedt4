import React from 'react';
import axios from 'axios';

export default class OMConfirmDeleteRooster extends React.Component{
    state = {currentRooster: null, hideSuccessMsg: true, hideErrorMsg: true}
    componentDidMount(){
        this.setState({
            currentRooster: this.props.location.state.currentRooster
        }) 
    }
    onClick = () => {
        console.log("test")
        axios.delete("http://localhost:8000/api/rooster/" + this.state.currentRooster).then((res) => {
            this.setState({
                hideSuccessMsg: false
            })
            setTimeout(() => {
                window.location = "/admin/tijden/roosters"
            }, 5000)
        }).catch(error => {
            this.setState({
                hideErrorMsg: false
            })
        })
    }
    render(){
        return (
            <article>
            <header className="OM__roosters__header">
                    <h2>Verwijder Rooster</h2>
            </header>
            <section className="OM__roosters__confirm">
                <p>Weet u zeker dat u dit rooster wilt verwijderen?</p>
                <section className="OM__roosters__confirm__btnContainer">
                <button className="OM__roosters__confirm__btnContainer__btn OM__roosters__confirm__btnContainer__btn--yes" onClick={() => this.onClick()}>Ja</button>
                <button className="OM__roosters__confirm__btnContainer__btn OM__roosters__confirm__btnContainer__btn--no">Nee</button>
                </section>
              
            </section>
            <section hidden={this.state.hideSuccessMsg} className="OM__roosters__form__successMsg">Rooster succesvol verwijderd!</section>
                <section hidden={this.state.hideErrorMsg} className="OM__roosters__form__errorMsg">Rooster kan niet worden verwijderd, door eventuele bestaande reserveringen/tijdsloten</section>
            </article>
        )
    }
}