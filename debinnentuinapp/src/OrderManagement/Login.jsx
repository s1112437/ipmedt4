import React from 'react';
import {connect} from "react-redux";
import { withRouter } from "react-router";
import {setAdminAuthenticated} from "../Redux/Actions";
import AXIOS from "../axios";
import {
    useHistory,
    useLocation,
    Redirect
  } from "react-router-dom";
class OMLogin extends React.Component{
    state = {isLoggedIn: false, error: '', user:{email:'', password:''}}

    
    handleEmail = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, email: value
            }
        })); 
    }

    handlePassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, password: value
            }
        })); 
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log("JAAAAAAAA")

        let userData = this.state.user;
        // axios.defaults.headers.common['CSRF-Token'] = "my CSRF-Token"
        AXIOS.post("http://localhost:8000/api/auth/login", {
            email: this.state.user.email,
            password: this.state.user.password,
           
            }).then(response => {
            return response;
            

        }).then(json => {
            if (json.status === 200){
                AXIOS.defaults.headers.common['CSRF-Token'] = json.data.csrf_token;

                AXIOS.get("http://localhost:8000/api/user/" + json.data.data.id).then(res => {
                    if(res.data.User.role === "Admin"){
                        this.props.setAdminAuthenticated(true)
    
                    }
                    else {
                        this.setState({
                            error: "Deze gebruiker heeft onvoldoende rechten"
                        })
                    }

                });

                
            }
        }).catch(error => {if (error.response) {
            // The request was made and the server responded with a status code that falls out of the range of 2xx
            let err = error.response.data;
            this.setState({
              error: err.message,
              errorMessage: err.errors,
              formSubmitting: false
            })
          }
          else if (error.request) {
            // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
            let err = error.request;
            this.setState({
              error: err,
              formSubmitting: false
            })
         } else {
           // Something happened in setting up the request that triggered an Error
           let err = error.message;
           this.setState({
             error: err,
             formSubmitting: false
           })
        //    console.log(this.state.error)
       }
     }).finally(this.setState({error: ''}));
    }

    handleClick = (path) =>{
        this.props.history.push(path)
    } 


    render(){
        if (this.props.isAdminAuthenticated) {
            const { from } = this.props.location.state || { from: { pathname: '/admin' } }
            return <Redirect to={from} />
          }
        return(
            <article className="OM__container">
                <section className="OM__container">
                    <header className="OM__container__header OM__login">
                    <h2>OrderManagement Login</h2>
                    </header>
                <form className="OM__login__form" onSubmit={this.handleSubmit}>
                    <p>{this.state.error}</p>
                    <label className="OM__login__form__label" name="email">E-mail</label>
                    <input onChange={this.handleEmail} className="OM__login__form__input" name="email" type="email" placeholder="Vul E-mail in" required/>
                    <label className="OM__login__form__label" name="password">Wachtwoord</label>
                    <input onChange={this.handlePassword} className="OM__login__form__input" pattern=".{6,}" name="password" type="password" placeholder="Vul wachtwoord in" required/>     
                    <button className="OM__login__form__button" type="submit" value="Submit">Login</button>
                   
                </form>
                </section>
            </article>
        )
    }
}
const mapStateToProps = state => {
    return { 
             isAdminAuthenticated: state.isAdminAuthenticated,
            };
}
export default withRouter(connect(mapStateToProps,{ setAdminAuthenticated: setAdminAuthenticated}) (OMLogin));