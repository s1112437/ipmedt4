import React from 'react';
import {connect} from "react-redux";
import {setCurrentOrderOM} from "../Redux/Actions";
class BestellingenViewItemsOM extends React.Component {
    
    setToDouble = prijs =>{
        return prijs.toFixed(2);
    }
    render(){
        const bestellingenItems = this.props.bestellingenList.map(bestellingen_items => <tr key={bestellingen_items}>
            <td>{bestellingen_items.bestelling_item_id}</td>
            <td>{bestellingen_items.bestelling_item_naam}</td>
            <td>{bestellingen_items.aantal}</td>
            <td>{this.setToDouble(bestellingen_items.prijs_per_stuk)}</td>
        </tr>)
        return (
        <tbody>
            {bestellingenItems}
        </tbody>
        )
    }
}

const mapStateToProps = state => {
    return {current_order_om: state.current_order_om}
}
export default connect(mapStateToProps, {setCurrentOrderOM: setCurrentOrderOM}) (BestellingenViewItemsOM);