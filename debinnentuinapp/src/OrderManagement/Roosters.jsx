    import React from 'react';
import axios from 'axios';
import OMRooster from './Rooster.jsx';
import { Provider } from "react-redux";
import { store } from "../Redux/Store";
import History from "./History"
 class OMRoosters extends React.Component {
    state = {roosters: []};
    componentDidMount() {
        axios.get("http://localhost:8000/api/roosters", {
            withCredentials: true
        }).then((res) => {
            
            res.data.forEach((rooster) => {
                
                axios.get("http://localhost:8000/api/location/" + rooster.location_id).then((res) => {
                    rooster["Locatie"] = res.data.Locatie;
                    console.log(rooster)
                    this.setState({
                        roosters: [...this.state.roosters, rooster]
                    })
                })

            });
        })
    };
     render(){

        return(
            <article className="OM__container">
            <section className="OM__container OM__roosters">
                <header className="OM__container__header">
                    <h2>Roosters</h2>
                    <a href="/admin/rooster/create"  className="OM__container__header__button">
                    <span className="OM__container__header__button__icon material-icons">
                        add
                    </span>
                        Rooster toevoegen
                    </a>

                </header>
                <table  className="OM__roosters__table">
                        <thead>
                        <tr>
                            <th scope="col">Naam</th>
                            <th scope="col">Locatie</th>
                            <th scope="col">Rooster startdatum</th>
                            <th scope="col">Rooster einddatum</th>
                            <th scope="col"></th>

                        </tr>

                        </thead>
                        <Provider store={store}>
                            <OMRooster goToEdit={(roosterId) => this.props.history.push({
            pathname: '/admin/rooster/edit',
            state: { currentRooster: roosterId }
        })} goToConfirmDelete={(roosterId) => this.props.history.push({
            pathname: '/admin/rooster/delete',
            state: { currentRooster: roosterId }
        })} roosterlist={this.state.roosters} history={this.props.history} />
                        </Provider>


   
   

                        
            
                </table>
            </section>
            </article>
        )
    }
}

export default OMRoosters;