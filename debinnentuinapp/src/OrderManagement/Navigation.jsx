import React from 'react';
import {connect} from "react-redux";
class OMNavigation extends React.Component {
    state = { activeDropdownElmId: null }
    openDropdownMenu = (e) => {
        e.target.childNodes[1].style.display = "flex"
        this.setState({
            activeDropdownElmId: e.target.childNodes[1].id
        })

    }
    closeDropdownMenu = () => {
        document.getElementById(this.state.activeDropdownElmId).style.display = "none";
    }
    navigate = (path) => {
        this.props.history.push(path)
    }

    render(){
        if(!this.props.isAdminAuthenticated){
            return null
        }
        return(
            document.getElementById('nav-admin').className ="OM__nav",
            <ul className="OM__nav__list" >
        <li className="OM__nav__list__item">
            Home
        </li>
        <a href="/admin/klanten" onMouseEnter={(e) => this.openDropdownMenu(e)} onMouseLeave={this.closeDropdownMenu} className="OM__nav__list__item">
            Klanten
            <section id="js--klanten" onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item__dropdown">
                <ul  className="OM__nav__list__item__dropdown__list">
                    <a href="/admin/klanten/invites" className={window.location.pathname === "/admin/klanten/invites" ? "OM__nav__list__item__dropdown__list__item OM__nav__list__item__dropdown__list__item--active" : "OM__nav__list__item__dropdown__list__item"}>
                        Community Invites
                    </a>

                </ul>
            </section>
        </a>

        <a href="/admin/bestellingen" className="OM__nav__list__item">
            Bestellingen
        </a>

        <a href="/admin/reserveringen" onMouseEnter={(e) => this.openDropdownMenu(e)} onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item">
            Reserveringen
            <section id="js--reserveringen" onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item__dropdown">
                <ul  className="OM__nav__list__item__dropdown__list">
                    <a href="/admin/reserveringenmap" className={window.location.pathname === "/admin/reserveringenmap" ? "OM__nav__list__item__dropdown__list__item OM__nav__list__item__dropdown__list__item--active" : "OM__nav__list__item__dropdown__list__item"}>
                        Tafelweergave
                    </a>
                </ul>
            </section>

        </a>
        <li onMouseEnter={(e) => this.openDropdownMenu(e)} onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item">
            Tijden
            <section id="js--tijdenDropdown" onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item__dropdown">
                <ul  className="OM__nav__list__item__dropdown__list">
                    <a href="/admin/tijden/roosters" className={window.location.pathname === "/admin/tijden/roosters" ? "OM__nav__list__item__dropdown__list__item OM__nav__list__item__dropdown__list__item--active" : "OM__nav__list__item__dropdown__list__item"}>
                        Roosters
                    </a>
                    <a href="/admin/tijden/tijdsloten" className={window.location.pathname === "/admin/tijden/tijdsloten" ? "OM__nav__list__item__dropdown__list__item OM__nav__list__item__dropdown__list__item--active" : "OM__nav__list__item__dropdown__list__item"}>
                        Tijdsloten
                    </a>
                </ul>
            </section>

        </li>
        <li onMouseEnter={(e) => this.openDropdownMenu(e)} onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item">
            Voorkeuren
            <section id="js--voorkeurenDropdown" onMouseLeave={this.closeDropdownMenu}  className="OM__nav__list__item__dropdown">
                <ul  className="OM__nav__list__item__dropdown__list">
                    <a href="/admin/mapeditor" className={window.location.pathname === "/admin/mapeditor" ? "OM__nav__list__item__dropdown__list__item OM__nav__list__item__dropdown__list__item--active" : "OM__nav__list__item__dropdown__list__item"}>
                        Map Editor
                    </a>
                </ul>
            </section>
            </li>
            </ul>
        );
    };
};
const mapStateToProps = state => {
    return { isAdminAuthenticated: state.isAdminAuthenticated}
}
export default connect(mapStateToProps) (OMNavigation)