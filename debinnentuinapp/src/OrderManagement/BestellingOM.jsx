import React from 'react';
import { connect } from "react-redux";
import { setCurrentOrderOM } from "../Redux/Actions";

class BestellingOM extends React.Component {
    setBestellingenOM = (bestellingen) => {
        // Paremeter wordt in de redux state gezet.
        this.props.setCurrentOrderOM(bestellingen);

        // Go to edit van de parent props functie wordt genitialiseerd.
        this.props.goToEdit(bestellingen);
    }
    
    setPrizeToDouble = prijs =>{
        return prijs.toFixed(2);
    }

    render(){
        // Hier wordt een bestelling van de props array uitgelezen en omgezet naar een tabelrij.
        const bestellingenList = this.props.bestellingenList.map(bestellingen => <tr key={bestellingen}>
            <td>{bestellingen.bestelling_id_order}</td>
            <td>{bestellingen.reservering_id}</td>
            <td>{bestellingen.klant_naam}</td>
            <td>{bestellingen.status}</td>
            <td>{this.setPrizeToDouble(bestellingen.totaal_prijs)}</td>
            <td>{bestellingen.extra_info}</td>
            <td>{bestellingen.created_at}</td>

            {/* Druk op de icon laadt bovenstaande functie. */}
            <td className="OM__bestellingen__table__icon">
                <a className="material-icons" onClick={() => 
                this.setBestellingenOM(
                    [
                        bestellingen.bestelling_id_order, 
                        bestellingen.reservering_id,
                        bestellingen.klant_naam,
                        bestellingen.status,
                        this.setPrizeToDouble(bestellingen.totaal_prijs),
                        bestellingen.extra_info,
                        bestellingen.created_at
                    ]
                    )}>
                    visibility
                </a>
            </td>

        </tr>)
        return (
        <tbody>
            {/* Gemaakte tabelrij wordt ingeladen */}
            {bestellingenList}
        </tbody>
        )
    }
}
const mapStateToProps = state => {
    return {current_order_om: state.current_order_om}
}
export default connect(mapStateToProps, {setCurrentOrderOM: setCurrentOrderOM}) (BestellingOM);