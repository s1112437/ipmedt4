import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import moment from 'moment';
export default class OMRoosterCreate extends React.Component{
    state = {locations: [], location_id: 1, hideSuccessMsg: true, hideErrorMsg: true}
    componentDidMount(){

        axios.get('http://localhost:8000/api/locations').then((res) => {
            this.setState({
                locations : res.data
            })
        })

    }
    handleInputChange = (event) =>{
        const target = event.target;
        const value = target.name === "oneindig" ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    onSubmit = (event) => {
        event.preventDefault();
        axios.post('http://localhost:8000/api/rooster', this.state).then((res) => {
            this.setState({
                hideSuccessMsg: false
            })
            setTimeout(() => {
                window.location = "/admin/tijden/roosters"
            }, 5000)
        }).catch(error => {
            this.setState({
                hideErrorMsg: false
            })

        });
    }
    render(){
        let Locations = this.state.locations.map(location => <option key={location.id} value={location.id} >{location.Locatie}</option> );

        return(
            <article>
            <header className="OM__roosters__header">
                    <h2>Rooster aanmaken</h2>
            </header>
            <form onSubmit={this.onSubmit} className="OM__roosters__form">
                <label for="name" className="OM__roosters__form__label">Naam Rooster</label>
                <input type="text" className="OM__roosters__form__input" name="name" onChange={this.handleInputChange} value={this.state.name} required></input>
                <label for="location" className="OM__roosters__form__label">Locatie</label>
                <select defaultValue={this.state.location_id} className="OM__roosters__form__input" onChange={this.handleInputChange} id="location" name="location_id" required>
                    {Locations}
                </select>
                <article className="OM__roosters__form__formset">
                    <section>
                        <label for="startDatum" className="OM__roosters__form__formset__label">Rooster startdatum</label>
                        <input disabled={this.state.oneindig ? 'true' : null} type="date" className="OM__roosters__form__formset__input" id="startDatum" name="startDatum" value={this.state.startDatum} onChange={this.handleInputChange}></input>
                    </section>
                    <section>
                        <label for="eindDatum" className="OM__roosters__form__formset__label">Rooster einddatum</label>
                        <input disabled={this.state.oneindig ? 'true' : null} type="date" className="OM__roosters__form__formset__input" id="eindDatum" name="eindDatum" value={this.state.eindDatum} onChange={this.handleInputChange}></input>
                    </section>
                </article>
                <label for="oneindig" className="OM__roosters__form__label">Oneidig rooster</label>
                <input className="OM__roosters__form__input" type="checkbox" id="oneindig" name="oneindig" defaultChecked={this.state.oneindig} value={this.state.oneindig} onChange={this.handleInputChange}></input>
                <label for="aantalPersonen" className="OM__roosters__form__label">Aantal personen</label>
                <input className="OM__roosters__form__input" type="number" id="aantalPersonen" name="aantalPersonen" value={this.state.aantalPersonen} onChange={this.handleInputChange} required></input>
                <article className="OM__roosters__form__formset">
                    <section>
                        <label for="duurTijdSlot" className="OM__roosters__form__formset__label">Duur tijdslot</label>
                        <input className="OM__roosters__form__formset__input" type="time" id="duurTijdSlot" name="duurTijdSlot" value={this.state.duurTijdSlot} step="2" onChange={this.handleInputChange} required></input>
                    </section>
                    <section>
                        <label for="duurTussenTijdSlot" className="OM__roosters__form__formset__label">Duur tussen tijdsloten</label>
                        <input className="OM__roosters__form__formset__input" type="time" id="duurTussenTijdSlot" name="duurTussenTijdSlot" value={this.state.duurTussenTijdSlot} step="2" onChange={this.handleInputChange} required></input>
                    </section>
                </article>
                <h3>Openingstijden Rooster</h3>
                <table className="OM__roosters__form__table">
                    <thead>
                        <th></th>
                        <th>Openingstijd</th>
                        <th>Sluitingstijd</th>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">Maandag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="monday_openingstijd" value={this.state.monday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="monday_sluitingstijd" value={this.state.monday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row">Dinsdag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="tuesday_openingstijd" value={this.state.tuesday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="tuesday_sluitingstijd" value={this.state.tuesday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row">Woensdag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="wednesday_openingstijd" value={this.state.wednesday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="wednesday_sluitingstijd" value={this.state.wednesday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row">Donderdag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="thursday_openingstijd" value={this.state.thursday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="thursday_sluitingstijd" value={this.state.thursday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row">Vrijdag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="friday_openingstijd" value={this.state.friday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="friday_sluitingstijd" value={this.state.friday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row">Zaterdag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="saturday_openingstijd" value={this.state.saturday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="saturday_sluitingstijd" value={this.state.saturday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row">Zondag</th>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="sunday_openingstijd" value={this.state.sunday_openingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        <td>
                            <input className="OM__roosters__form__table__input" type="time" step="2" name="sunday_sluitingstijd" value={this.state.sunday_sluitingstijd} onChange={this.handleInputChange}></input>
                        </td>
                        </tr>
                        
                    </tbody>
                </table>
                <input className="OM__roosters__form__submit" type="submit" name="sumbit" value="Opslaan"></input>
                <section hidden={this.state.hideSuccessMsg} className="OM__roosters__form__successMsg">Rooster succesvol opgeslagen!</section>
                <section hidden={this.state.hideErrorMsg} className="OM__roosters__form__errorMsg">Er is iets fout gegaan!</section>
            </form>
            </article>


        );
    }
}

if (document.getElementById('om-roostercreate')) {
    ReactDOM.render(<OMRoosterCreate />, document.getElementById('om-roostercreate'));
}