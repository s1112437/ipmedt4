import React from 'react';
import moment from 'moment';
import axios from 'axios';
import Reservering from './Tijd';
class OMReserveringen extends React.Component{
    state = {tijden: [], date: moment().format(), selectedLocation: 1, locations: []};
    componentDidMount(){
        this.getTijden();
        this.getLocations();
        
    }

    getTijden() {
        this.setState({
            tijden: []
        })
        axios.get("http://localhost:8000/api/openingstijdenbylocation/" + this.state.selectedLocation + "/" + moment(this.state.date).format("dddd")).then((res) => {
        let starttijd = moment(moment(this.state.date).format("YYYY-MM-DD") + ' ' + res.data[0].startTijd);
        let eindTijd =moment(moment(this.state.date).format("YYYY-MM-DD") + ' ' + res.data[0].eindTijd);
        let diffDuration = moment.duration(eindTijd.diff(starttijd))
        let hours = diffDuration.hours();

        this.setState({
            tijden: [...this.state.tijden, starttijd.startOf('hour').format("HH:mm")]
        })
        for(let i = 0; i < hours;i++){
            this.setState({
                tijden: [...this.state.tijden, starttijd.startOf('hour').add(1,'hour').format("HH:mm")]
            })
        }

        })
        
    }
    addDay = (event) => {
        event.preventDefault();
        this.setState({
            date: moment(this.state.date).add(1,"days")
        }, () => {
            this.getTijden();
        });
        

    }
    removeDay = (event) => {
        event.preventDefault();
        this.setState({
            date: moment(this.state.date).add(-1,"days")
        }, () => {
            this.getTijden();
        });
        
    }
    getLocations = () => {
        axios.get("http://localhost:8000/api/locations/").then((res) => {
            this.setState({
                locations: res.data
            })
        })
    }
    selectLocation = (e) => {
        this.setState({
            selectedLocation: e.target.value
        }, () => {
            this.getTijden()
        })
    }
    render(){
        let tijdenElms;

        tijdenElms = this.state.tijden.map((tijd) => <Reservering history={this.props.history} selectedLocation={this.state.selectedLocation} tijd={tijd} date={this.state.date} />)
        return(
             <article className="OM__container">
                <section className="OM__container OM__reserveringen">
                    <header className="OM__container__header">
                    <h2>Reserveringen</h2>
                    <article className="OM__container__header__filterContainer">
                    <label className="OM__container__header__filterContainer__label" for="location">Locatie:</label>
                        <select className="OM__container__header__filterContainer__input" id="location" name="location" value={this.state.selectedLocation} onChange={this.selectLocation}>
                            {this.state.locations.map(location => <option value={location.id} >{location.Locatie}</option>)}
                        </select>
                        <button onClick={this.removeDay} className="OM__container__header__filterContainer__prev material-icons">
                        navigate_before
                        </button>
                        <section className="OM__container__header__filterContainer__date ">
                        {moment(this.state.date).format("DD-MM-YYYY")}
                        </section>
                        <button onClick={this.addDay} className="OM__container__header__filterContainer__next material-icons">
                        navigate_next
                        </button>
                    </article>
                    </header>
                

                        <ul className="OM__reserveringen__list">
                            {tijdenElms}
                        </ul>
                        </section>
                    </article>

        )
    }
}

export default OMReserveringen