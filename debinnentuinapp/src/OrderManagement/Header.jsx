import React from 'react';
import ReactDOM from 'react-dom';
import "../style/OrderManagement/appAdmin.css";
import { connect } from 'react-redux';
import AXIOS from "../axios";
import { setAdminAuthenticated } from '../Redux/Actions';

 function OMHeader(props){
    if(!props.isAdminAuthenticated){
        return null
    }

    return(
        <article className="OM__header">
            <img src="https://rubenjerry.nl/wp-content/uploads/2019/05/logo_green-300x67.png"></img>
            <article className="OM__header__details" >
                <section onClick={() => AXIOS.post("http://localhost:8000/api/auth/logout").then((res) => { props.setAdminAuthenticated(false); document.location = "/admin/login";})} className="OM__header__details__signout">
                <span className="material-icons">
                    exit_to_app
                </span>
                Uitloggen
                </section>
            </article>
        </article>
    )
};
const mapStateToProps = state => {
    return {isAdminAuthenticated: state.isAdminAuthenticated}
}
export default connect(mapStateToProps, {setAdminAuthenticated: setAdminAuthenticated}) (OMHeader)