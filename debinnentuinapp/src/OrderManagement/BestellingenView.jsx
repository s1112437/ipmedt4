import React from 'react';
import axios from 'axios';
import { connect } from "react-redux";
import { setCurrentOrderOM } from "../Redux/Actions";
import { Provider } from "react-redux";
import { store } from "../Redux/Store";
import BestellingenViewItem from './BestellingenViewItem.jsx';

 class OMBestellingenView extends React.Component {
    state = {bestellingen_items: []};

    // Order Items data die bij de gekozen order horen worden eenmaal gehaald uit de api. Deze worden alsnog bewaard in de state.
    componentDidMount() {
        axios.get("http://localhost:8000/api/bestellen/orders/" + this.props.current_order_om[0], {
            withCredentials: true
        }).then((res) => {
            res.data.forEach((bestelling_order) => {
                this.setState({
                    bestellingen_items: [...this.state.bestellingen_items, bestelling_order]
                }); 
            });
        });
    }

    render(){
    return(
        <article className="OM__container">
            <section className="OM__container OM__bestellingen">
                <header className="OM__container__header">
                    <h2>Bestelling {this.props.current_order_om[0]}:</h2>
                </header>

                {/* Gekozen Bestelling Tabel */}
                <table className="OM__bestellingen__table">
                    <thead>
                        <tr>
                            <th scope="col">BestellingID</th>
                            <th scope="col">ReserveringID</th>
                            <th scope="col">Klant</th>
                            <th scope="col">Status</th>
                            <th scope="col">Totaal Prijs</th>
                            <th scope="col">Extra Info</th>
                            <th scope="col">Gemaakt in</th>
                            <th scope="col"></th>
                        </tr>
                        
                        
                    </thead>    
                    {/* Gekozen Bestelling informatie */}
                    <tr>
                            <td>{this.props.current_order_om[0]}</td>
                            <td>{this.props.current_order_om[1]}</td>
                            <td>{this.props.current_order_om[2]}</td>
                            <td>{this.props.current_order_om[3]}</td>
                            <td>{this.props.current_order_om[4]}</td>
                            <td>{this.props.current_order_om[5]}</td>
                            <td>{this.props.current_order_om[6]}</td>
                        </tr>   
                </table>
                
                <header className="OM__container__header">
                    <h3 style={{fontSize: '1.9em'}}>Bestelling Items: </h3>
                </header>

                {/* Gekozen Bestelling Items Tabel*/}
                <table className="OM__bestellingen__table">
                    <thead>
                        <tr>
                            <th scope="col">Item Id</th>
                            <th scope="col">Item Naam</th>
                            <th scope="col">Aantal</th>
                            <th scope="col">Prijs Per Stuk</th>
                        </tr>   
                    </thead>
                    {/* Itereert per item in de bestelling items en maakt een rij per object */}
                    <Provider store={store}>
                        <BestellingenViewItem goToEdit={(bestelling_id_order) => this.props.history.push({
                            state:  {current_order_om: bestelling_id_order},
                        })} bestellingenList={this.state.bestellingen_items} history={this.props.history} />
                    </Provider>
                </table>
            </section>
        </article>
        )
    }
}

const mapStateToProps = state => {
    return {current_order_om: state.current_order_om}
}
export default connect(mapStateToProps, {setCurrentOrderOM: setCurrentOrderOM}) (OMBestellingenView);