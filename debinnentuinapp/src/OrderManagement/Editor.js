/**
 * @author mrdoob / http://mrdoob.com
 */

export default class Editor {

	constructor( svg ) {

		this.svg = svg;
		this.source = null;

	}

	addElement( element ) {

		this.svg.appendChild( element );
		this.svg.appendChild( document.createTextNode( '\n' ) );

		this.source.setText( this.toString() );

	}

	setSource( source ) {

		this.source = source;

	}

	setSVG( svg ) {

		this.svg.innerHTML = svg.documentElement.innerHTML;
		// this.source.setText( this.toString() );

	}

	clear() {

		this.svg.textContent = '';
		this.source.setText( this.toString() );

	}

	toString() {

		// TODO Checkbox for auto-formating

		return [
			'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 870 600">\n',
			this.svg.innerHTML,
			'</svg>'
		].join( '' );




	}

}
