import React from 'react';
import axios from 'axios';
import moment from 'moment';

class OMReserveringenMap extends React.Component{
    state = {selectedLocation: 1, locations: []}
    componentDidMount() {
        this.getLocations();
        this.getMap();

        
        
    }
    selectLocation = (e) =>{
        console.log(e.target.value)
        this.setState({
            selectedLocation: e.target.value
        }, () => {
            this.getMap();
        })
    }
    selectTable = (tableId) => {
        console.log(tableId)
        axios.get("http://localhost:8000/api/reserveringenbytable/" + tableId + "/" + moment().format("DD-MM-YYYY HH:mm:ss")).then((res) => {
            res.data.forEach((reservering) => {
                console.log(reservering)
                this.props.history.push({
                    pathname: '/admin/reservering',
                    state: { reserveringId: reservering.id }
                  })
            })
    })
}

    getReserveringen = () => {


        axios.get("http://localhost:8000/api/reserveringen/" + moment().format("DD-MM-YYYY HH:mm:ss")).then((res) => {
            res.data.forEach((reservering) => {
                let tables = document.getElementsByClassName('tables')
                for (var i = 0; i < tables.length; i++) {
                    let id = tables[i].attributes.id.value.replace("js--table-","");
                    if(reservering.tables_id = parseInt(id)){
                        console.log('Ja')
                        tables[i].style.fill = "red";
                    }
                }
            })

        })

        }
        

    getLocations = () => {
        axios.get("http://localhost:8000/api/locations/").then((res) => {
            this.setState({
                locations: res.data
            })
        })
    }
    getMap = () => {
        axios.get("http://localhost:8000/api/maps/" + this.state.selectedLocation, ).then((res) => {
            if(res.data !== ""){
                document.getElementById('js--content').innerHTML = new DOMParser().parseFromString( res.data.map, 'image/svg+xml' ).documentElement.innerHTML;
            }
            else{
                document.getElementById('js--content').innerHTML= new DOMParser().parseFromString( "", 'image/svg+xml' ).documentElement.innerHTML;
            }
        }).then(() => {

            let tables = document.getElementsByClassName('tables')
            console.log(tables)
            for (var i = 0; i < tables.length; i++) {
                let id = tables[i].attributes.id.value.replace("js--table-","");
                console.log(id)
                tables[i].addEventListener('click', () => this.selectTable(parseInt(id)));
            }
            this.getReserveringen();
        })
        
        
    }
    render() {

        return (
            <article className="OM__container">
                <section className="OM__container OM__reserveringen">
                    <header className="OM__container__header">
                    <h2>Reserveringen</h2>
                    <article className="OM__container__header__filterContainer">
                    <label className="OM__container__header__filterContainer__label" htmlFor="location">Locatie:</label>
                        <select className="OM__container__header__filterContainer__input" id="location" name="location" value={this.state.selectedLocation} onChange={this.selectLocation}>
                            {this.state.locations.map(location => <option value={location.id} >{location.Locatie}</option>)}
                        </select>
</article>
                    </header>
                    </section>
                    <section className="OM__ReserveringenMap">
                        <svg className="OM__ReserveringenMap__content" id="js--content" width="870" height="600" viewBox="0 0 870 600"></svg>
                    </section>
                    </article>
        )
    }
}

export default OMReserveringenMap;