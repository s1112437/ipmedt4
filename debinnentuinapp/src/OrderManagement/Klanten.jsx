import React from 'react';
import axios from 'axios';
import KlantenOM from './KlantenOM';
import { store } from "../Redux/Store";
import { Provider } from 'react-redux';




class Klanten extends React.Component {
    state = {klanten: []};


    //alle klanten worden uit de databse gehaald en in de state gezet.
    componentDidMount() {
        axios.get("http://localhost:8000/api/klanten", {
            withCredentials: true
        }).then((res) => {
            res.data.forEach((klanten_info) => {
                this.setState({
                    klanten: [...this.state.klanten, klanten_info]
                });
            });

        })
    };




    render(){
        console.log(this.state.klanten);
        return(
            <article className="OM__container">
                <section className="OM__container OM__klanten">
                    <header className="OM__container__header">
                        <h2>Klanten</h2>

                    </header>
                </section>
                <table  className="OM__klanten__table">
                    <thead>
                        <tr>
                            <th scope="col">Naam</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Telefoon nummer</th>
                            <th scope="col">Community korting</th>
                        </tr>
                    </thead>
                    <Provider store={store}>
                        <KlantenOM gotoEdit={(klanten) => this.props.history.push({
                            state: {klanten: klanten},
                        })} klantenList={this.state.klanten} history={this.props.history}
                        />
                    </Provider>
                </table>
            </article>
        )
    }
}


export default (Klanten);