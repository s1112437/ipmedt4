import React from 'react';
import axios from 'axios';
import moment from 'moment';
import BestellingReserveringOM from './BestellingReserveringOM.jsx';
import { Provider } from "react-redux";
import { store } from "../Redux/Store";
class OMShowReservering extends React.Component{
    state = {reserveringId: null, reservering: [],customer: [],bestellingen: [], tijdslot: [], location: [], table: [], availableTables: [], editTable: true, newTable: null}
    componentDidMount(){
        this.setState({
            reserveringId: this.props.location.state.reserveringId
        }, () => {
            this.getReservering();
        })
        
    }
    getReservering = () => {
        axios.get("http://localhost:8000/api/reservering/" + this.state.reserveringId).then((res) => {
            console.log(res.data)
        this.setState({
                reservering:  res.data.Reservering,
                tijdslot: res.data.timeslots
            },() => {
                this.getTijdslot();
                this.getTable();
                this.getCustomer();
                this.getBestellingen();
            })})
        }
    getTijdslot = () => {
        axios.get(this.state.tijdslot).then((res) => {
            console.log(res.data)
        this.setState({
                tijdslot: res.data.Tijdslot,
                rooster: res.data.Rooster
            }, () => {
                this.getLocation();
            })
        })
    }
    getLocation = () => {
        axios.get(this.state.rooster).then((res) => {
            console.log(res.data)

            axios.get(res.data.Location).then((res) => {
                this.setState({
                    location: res.data
                }, () => {
                    this.getTables();
                })
            })
        })
    }
    getTable = () => {
        axios.get("http://localhost:8000/api/tables/" + this.state.reservering.tables_id).then((res) => {
            this.setState({
                table: res.data
            })
        })
    }
    getTables = () => {
        axios.get("http://localhost:8000/api/availableTables/" + this.state.tijdslot.id + "/" + this.state.location.id).then((res) => {
            this.setState({
                availableTables: res.data
            })
        })
    }
    editTable = () => {
        this.setState({
            editTable: !this.state.editTable
        })
    }
    selectTable = (e) => {
        this.setState({
            newTable: e.target.value
        }, () => {
            axios.post("http://localhost:8000/api/reserveringsettable/" + this.state.reservering.id + "/" + this.state.newTable).then((res) => {
                this.setState({
                    editTable: true
                }, () => {
                    this.getReservering();
                })
            })
        })

    }
    getCustomer = () => {
        axios.get("http://localhost:8000/api/klant/" + this.state.reservering.klanten_id).then(res => {
            this.setState({
                customer: res.data
            })

        })
    }
    getBestellingen = () => {
        axios.get("http://localhost:8000/api/bestellingenbyreservering/" + this.state.reservering.id).then(res => {
            res.data.forEach((bestelling_order) => {
                this.setState({
                    bestellingen: [...this.state.bestellingen, bestelling_order]
                }); 
            });

        })
    }
    render(){
        return (
            <article className="OM__container">
                <section className="OM__container">
                    <header className="OM__container__header">
                    <h2>Reservering weergeven</h2>
                    </header>
                </section>
                <article className="OM__reservering">
                <article className="OM__reservering__details">
                    <section className="OM__reservering__details__column">
                    <h3>Reserveringsdetails</h3>
                    <table>
                        <tr>
                            <th>Reserveringsnummer</th>
                            <td>{this.state.reservering.id}</td>
                        </tr>
                        <tr>
                            <th>Gekozen tijd</th>
                            <td>{moment(this.state.tijdslot.startTijd, "HH:mm:ss").format("HH:mm") + " - " + moment(this.state.tijdslot.eindTijd, "HH:mm:ss").format("HH:mm")}</td>
                        </tr>
                        <tr>
                            <th>Aantal personen</th>
                            <td>{this.state.reservering.aantalPersonen}</td>
                        </tr>
                        <tr>
                            <th>Locatie</th>
                            <td>{this.state.location.Locatie}</td>
                        </tr>
                        <tr>
                            <th>Tafel</th>
                            <td hidden={!this.state.editTable} >{this.state.table.name} <section onClick={this.editTable} className="OM__reservering__details__column__icon material-icons">edit</section></td>
                            <td hidden={this.state.editTable}><select onChange={this.selectTable} value={this.state.newTable}>
                                <option value="">Selecteer tafel</option>

                                {this.state.availableTables.map(table => <option value={table.id}>{table.name}</option>)}
                                </select></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>{this.state.reservering.Status}</td>
                        </tr>
                    </table>

                    </section>
                    <section className="OM__reservering__details__column">
                    <h3>Klanten details</h3>
                    <table>
                        <tr>
                            <th>Naam</th>
                            <td>{this.state.customer.name}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{this.state.customer.email}</td>
                        </tr>
                        <tr>
                            <th>Telefoonnummer</th>
                            <td>{this.state.customer.phone}</td>
                        </tr>
                        <tr>
                            <th>Community lid</th>
                            <td>{this.state.customer.community_lid}</td>
                        </tr>
                    </table>

                    </section>
                    <section className="OM__reservering__details__column OM__reservering__details__column--colspan">
                    <h3>Bestellingen</h3>
                    <table className="OM__reservering__details__column__bestellingTable">
                        <thead>
                        <tr>
                                <th scope="col">Gemaakt op</th>
                                <th scope="col">Totaal Prijs</th>
                                <th scope="col">Status</th>
                                <th scope="col">Extra Info</th>
                                
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <Provider store={store}>
                            <BestellingReserveringOM goToEdit={(bestellingen) => this.props.history.push({
                                pathname: '/admin/bestelling/items',
                                state:  {bestellingen: bestellingen},
                                })} bestellingenList={this.state.bestellingen} history={this.props.history} 
                            />
                        </Provider>
                    </table>

                    </section>

                </article>
               
                </article>
                
            </article>
        )
    }
}
export default OMShowReservering;