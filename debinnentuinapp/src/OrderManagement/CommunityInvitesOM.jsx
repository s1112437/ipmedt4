import React from 'react';
import axios from 'axios';

import { store } from "../Redux/Store";
import { connect } from 'react-redux';


class CommunityOM extends React.Component{


    render(){
        //Hier wordt een klant uitgelezen  van de props uitgelzen en omgezet naar een tabel rij.
        const communityInvitesList = this.props.communityInvitesList.map(communityInvites => <tr key={communityInvites}>
            <td>{communityInvites.email}</td>
            <td>{communityInvites.verified}</td>    
            </tr>)
    return (
        <tbody>
            {/* tabelrij ingeladen */}
            {communityInvitesList}
        </tbody>
        )   
    }
}
const mapStateToProps = state => {
    return {communityInvites: state.communityInvites}
}
export default connect(mapStateToProps, {})(CommunityOM);