import React from 'react';

import axios from 'axios';
import { connect } from "react-redux";
import { setMessage } from "../Redux/Actions";

class OMTijdslot extends React.Component {
    setMessage = (type) =>  {
        if(type === "Success"){
            console.log("testmsg")
            this.props.setMessage("Success","Opgeslagen!")
        }
        else {
            this.props.setMessage("","")

        }
    };
    onClickAvailable (value, tijdslotId){

        axios.patch("http://localhost:8000/api/timeslots", {
            id: tijdslotId,
            beschikbaar: !value
        }).then(res => {
            if(res.status === 200){
                this.props.getTijdsloten();
                this.setMessage( "Success");
                setTimeout(() => {
                    this.setMessage()
                },3000)
            }
        });
    };

    onClickAmountPersons = (e) =>{
        
        console.log(e.target.childNodes[0].value)
        console.log(e.target.childNodes[1].value)

        e.preventDefault();
        axios.patch("http://localhost:8000/api/timeslots", {
            id: e.target.childNodes[1].value,
            aantalPersonen: e.target.childNodes[0].value
        }).then(res => {
            if(res.status === 200){
                this.props.getTijdsloten();
                this.setMessage( "Success");
                setTimeout(() => {
                    this.setMessage()
                },3000)
            }
        });
    }
    render(){
        const tijdslotList = this.props.tijdslotList.map(tijdslot => <tr key={tijdslot}>
            <td>{tijdslot.startTijd}</td>
            <td>{tijdslot.eindTijd}</td>
            <td><form onSubmit={this.onClickAmountPersons}><input type="number" name="aantalPersonen" defaultValue={tijdslot.aantalPersonen}></input><input type="hidden" name="tijdslotId" defaultValue={tijdslot.id}></input></form></td>
            <td><input type="checkbox" onChange={() => this.onClickAvailable(tijdslot.beschikbaar, tijdslot.id)} defaultChecked={tijdslot.beschikbaar}></input><i className="OM__tijdsloten__table__successMsg" id="js--successAvailable"></i></td>
            <td className="OM__tijdsloten__table__icon">
            </td>
        </tr>)
        return (
        <tbody>
            {tijdslotList}
        </tbody>
        )
    }
}
const mapStateToProps = state => {
    return { message: state.message}
}
export default connect(mapStateToProps,{setMessage: setMessage}) (OMTijdslot);