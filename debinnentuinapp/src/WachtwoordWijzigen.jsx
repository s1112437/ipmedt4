import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import AXIOS from "./axios";

class WachtwoordWijzigen extends React.Component{

    state = {user:null, newPassword: null, confirmNewPassword: null}

    componentDidMount(){
        this.props.setHeaderTitle("Wachtwoord wijzigen")
        this.checkAuthenticate()
    }

    checkAuthenticate = () =>  {
        AXIOS.get("http://localhost:8000/api/auth/" + 'check-authenticate').then((res) => {
            if(res.status === 200){
                this.setState({
                    user: res.data.user.id
                });
            }
        }).catch(error => {if (error.response) {
            console.log("Unauthorized")
        }
    })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("Test")
        AXIOS.patch("http://localhost:8000/api/veranderWachtwoord", {
            newPassword: this.state.newPassword,
            confirmNewPassword: this.state.confirmNewPassword,
            user_id: this.state.user
        }).then(response => {
            if(response.status === 200){
                this.props.history.push('/profiel')
            }
            else {
                return response;
            }
    });
}
    
    handleClick = (path) =>{
        this.props.history.push(path)
    } 

    changeNewPassword = (e) => {
        this.setState({
            newPassword: e.target.value
        });
    }
    changeConfirmNewPassword = (e) => {
        this.setState({
            confirmNewPassword: e.target.value
        });
    }


    render(){
        return(
            <article className ="container wachtwoordWijzigen">
                {setHeaderTitle}

                <form className="wachtwoordWijzigen__form" onSubmit={this.handleSubmit}>

                    <label className="wachtwoordWijzigen__form__label" name="password_new">Nieuw wachtwoord</label>
                    <input className="wachtwoordWijzigen__form__input" onChange={this.changeNewPassword} name="password_new" type="password" placeholder="Minimaal 6 karakters"></input>
                    <label className="wachtwoordWijzigen__form__label" name="password_bevestig">Wachtwoord bevestigen</label>
                    <input className="wachtwoordWijzigen__form__input"  onChange={this.changeConfirmNewPassword} name="password_bevestig" type="password" placeholder="Bevestig wachtwoord"></input>
                   
                    <button className="profiel__form__button" type="submit" value="submit">Opslaan</button>     
                </form>

                <section className="wachtwoordWijzigen__linkcontainer">
                    <a className="wachtwoordWijzigen__linkcontainer__link" onClick={() => this.handleClick("/Profiel")}>Terug naar profiel</a>  
                </section>

            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle };
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle}) (WachtwoordWijzigen);
